<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
		<meta name="viewport" content="width=1200">
        <title>Anasayfa ~ eHaber.com</title>
        <link href="assets/css/ehaber.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300&subset=latin,latin-ext" rel="stylesheet" type="text/css"/>
    </head>	
    <body>
        <div class="cerceve">
            <header class="ust">
                <div class="headerUstTutucu">
                    <div class="headerUst">
                        <div class="tutucu">
                            <div class="logo sol">
                                <h1>
                                    <a href="#"><img src="assets/img/logohaberler.png" alt="eHaber"/></a>
                                </h1>
                            </div><!-- /. logo -->
                            <nav>
                                <ul>
                                    <li>
                                        <a href="#">HABERLER</a>
                                    </li>
                                    <li>
                                        <a href="#">YEREL</a>
                                    </li>
                                    <li>
                                        <a href="#">GÜNDEM</a>
                                    </li>
                                    <li>
                                        <a href="#">EKONOMİ</a>
                                    </li>
                                    <li>
                                        <a href="#">MAGAZİN</a>
                                    </li>
                                    <li>
                                        <a href="#">TEKNOLOJİ</a>
                                    </li>
                                    <li>
                                        <a href="#">SPOR</a>
                                    </li>
                                    <li>
                                        <a href="#">WEB TV</a>
                                    </li>
                                    <li>
                                        <a href="#">GALERİ</a>
                                    </li>
                                </ul>
                            </nav>
                        </div><!-- /. tutucu -->
                    </div><!-- /. headerUst -->
                </div><!-- /. headerUstTutucu -->
                <div class="havaDurumuUyeGirisi">
                    <div class="tutucu">
                        <div class="guncelDoviz">
                            <div class="doviz usd">
                                <span><i class="fa fa-arrow-up"></i><strong>USD</strong>2,9194</span>
                            </div><!-- /. doviz -->
                            <div class="doviz euro">
                                <span><i class="fa fa-arrow-down"></i><strong>EURO</strong>3,1701</span>
                            </div><!-- /. doviz -->
                            <div class="doviz altin">
                                <span><i class="fa fa-minus"></i><strong>ALTIN</strong>100,1300</span>
                            </div><!-- /. doviz -->
                            <div class="doviz bist">
                                <span><i class="fa fa-arrow-up"></i><strong>BİST 100</strong>87,0255</span>
                            </div><!-- /. doviz -->
                        </div><!-- /. guncelDoviz -->
                        <div class="havaDurumu">
                            <span class="sehir"><i class="sprite icon44"></i><strong class="sehirIsmi">İSTANBUL</strong><strong class="sehirHavasi">Parçalı bulutlu</strong></span>
                            <span class="sicaklik">22<strong>°C</strong></span>
                            <a href="#" class="tumSehirler"><i class="fa fa-angle-down"></i></a>
                            <ul class="tumSehirlerListe">
                                <li><a href="#">Svenska</a></li>
                                <li><a href="#">İstanbul</a></li>
                                <li><a href="#">Winaray</a></li>
                                <li><a href="#">Romani</a></li>
                                <li><a href="#">Nederlands</a></li>
                                <li><a href="#">Latina</a></li>
                                <li><a href="#">Ladino</a></li>
                                <li><a href="#">Español</a></li>
                                <li><a href="#">Eesti</a></li>
                                <li><a href="#">Euskara</a></li>
                            </ul>
                        </div><!-- /. havaDurumu -->
                        <div class="aramaBolumuTutucu">
                            <div class="aramaBolumu">
                                <div class="aramaOrtala">
                                    <div class="inputTutucu">
                                        <input type="text" placeholder="Aradığınız kelimeyi yazınız" />
                                        <i class="fa fa-times"></i>
                                        <i class="fa fa-search"></i>
                                    </div><!-- /. inputTutucu -->
                                    <span class="sonuc">1.025 sonuç</span>
                                </div><!-- /. aramaOrtala -->
                                <a class="aramaKapat" href="#">KAPAT</a>
                            </div><!-- /. aramaBolumu -->
                        </div><!-- /. aramaBolumuTutucu -->
                        <div class="uyePanel">
                            <a class="yeniUye" onclick="uyeGirisiPop.open();" href="#"><i class="fa fa-plus-circle"></i>Yeni Üyelik</a>
                            <a class="uyeGiris" onclick="uyeGirisiPop.open();" href="#"><i class="fa fa-user"></i>Üye Girişi</a>
                        </div><!-- /. uyePanel -->
                    </div><!-- /. tutucu -->
                    <div class="aramaDetayBolumu">
                        <div class="tutucu">
                            <div class="aramaBolumuSol">
                                <div class="blok">
                                    <span class="bolumBaslik">BÖLÜMLER</span>
                                    <ul>
                                        <li class="kirmizi"><a class="aktif" href="#">Haber<span>43</span></a></li>
                                        <li class="yesil"><a href="#">Spor<span>4</span></a></li>
                                        <li class="turuncu"><a href="#">Video<span>15</span></a></li>
                                        <li class="mavi"><a href="#">Galeri<span>624</span></a></li>
                                        <li class="kirmizi"><a href="#">Biyografi<span>10</span></a></li>
                                    </ul>
                                </div><!-- /. blok -->
                                <div class="blok">
                                    <span class="bolumBaslik">TARİH</span>
                                    <ul>
                                        <li><a class="aktif" href="#">Son bir saat <i class="fa fa-times"></i></a></li>
                                        <li><a href="#">Bugün <i class="fa fa-times"></i></a></li>
                                        <li><a href="#">Bu Hafta <i class="fa fa-times"></i></a></li>
                                        <li><a href="#">Bu Ay <i class="fa fa-times"></i></a></li>
                                        <li><a href="#">Bu Yıl <i class="fa fa-times"></i></a></li>
                                    </ul>
                                </div><!-- /. blok -->
                                <div class="blok">
                                    <span class="bolumBaslik">POPÜLERİTE</span>
                                    <ul>
                                        <li><a class="aktif" href="#">En çok tıklananlar <i class="fa fa-times"></i></a></li>
                                        <li><a href="#">En çok yorumlananlar <i class="fa fa-times"></i></a></li>
                                        <li><a href="#">En çok beğenilenler <i class="fa fa-times"></i></a></li>
                                    </ul>
                                </div><!-- /. blok -->
                            </div><!-- /. aramaBolumuSol -->
                            <div class="aramaBolumuSag">
                                <div class="nano">
                                    <div class="nano-content">
                                        <div class="haberler">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama1.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama2.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama4.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama1.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama2.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama4.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama1.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama2.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama4.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama1.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama2.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama3.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="imgTutucu">
                                                            <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/arama4.jpg" alt="1"/>
                                                        </div><!-- /. imgTutucu -->
                                                        <div class="detay">
                                                            <h2 class="baslik">İlk Kez Tanıtılan Elektrikli Minibüse Bakandan Ziyaret</h2>
                                                            <span class="goruntulenme">10.718 görüntüleme - 1 gün önce</span>
                                                        </div><!-- /. detay -->
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div><!-- /. haberler -->
                                    </div><!-- /. nano-content -->
                                </div><!-- /. nano -->
                            </div><!-- /. aramaBolumuSag -->
                        </div><!-- /. tutucu -->
                    </div><!-- /. aramaDetayBolumu -->
                </div><!-- /. havaDurumuUyeGirisi -->
                <div class="menu">
                    <div class="tutucu">
                        <nav>
                            <ul class="menu">
                                <li class="sonDakika"><a href="#" class="buyuk"><i class="fa fa-exclamation-triangle"></i>Son Dakika</a></li>
                                <li class="gundem"><a href="#" class="buyuk"><i class="fa fa-clock-o"></i>Gündem</a></li>
                                <li class="mansetler"><a href="#" class="buyuk"><i class="fa fa-star-o"></i>Manşetler</a></li>
                                <li class="line"></li>
                                <li class="dunya"><a href="#" class="kucuk"><i class="fa fa-globe"></i>Dünya</a></li>
                                <li class="ekonomi"><a href="#" class="kucuk"><i class="fa fa-bar-chart"></i>Ekonomi</a></li>
                                <li class="siyaset"><a href="#" class="kucuk"><i class="fa fa-university" aria-hidden="true"></i>Siyaset</a></li>
                                <li class="magazin"><a href="#" class="kucuk"><i class="fa fa-slideshare"></i>Magazin</a></li>
                                <li class="tumu">
                                    <a href="#"  class="kucuk tumu"><i class="fa fa-list-ul"></i>Tümü<i class="fa fa-angle-down"></i></a>
                                    <div class="tumMenuler">
                                        <ul class="buyukBasliklar">
                                            <li><a href="#"><i class="fa fa-exclamation-triangle"></i>Çeşitli Kategori</a></li>
                                            <li><a href="#"><i class="fa fa-clock-o"></i>Başlıklar Buraya</a></li>
                                            <li><a href="#"><i class="fa fa-star-o"></i>Denem Burası</a></li>
                                            <li><a href="#"><i class="fa fa-globe"></i>Yerel Haber</a></li>
                                            <li><a href="#"><i class="fa fa-list-ul"></i>Uçan Kuşlar</a></li>
                                            <li><a href="#"><i class="fa fa-balance-scale"></i>Magazin</a></li>
                                            <li><a href="#"><i class="fa fa-chrome"></i>Denme</a></li>
                                            <li><a href="#"><i class="fa fa-list-ul"></i>Yaz Haberi</a></li>
                                            <li><a href="#"><i class="fa fa-exclamation-triangle"></i>Seyehat</a></li>
                                            <li><a href="#"><i class="fa fa-clock-o"></i>Çeşitli Kategori</a></li>
                                            <li><a href="#"><i class="fa fa-star-o"></i>Başlıklar Buraya</a></li>
                                            <li><a href="#"><i class="fa fa-globe"></i>Denem Burası</a></li>
                                        </ul>
                                        <ul class="kucukBasliklar">
                                            <li><a href="#">Çeşitli Kategori</a></li>
                                            <li><a href="#">Başlıklar Buraya</a></li>
                                            <li><a href="#">Denem Burası</a></li>
                                            <li><a href="#">Yerel Haber</a></li>
                                            <li><a href="#">Uçan Kuşlar</a></li>
                                            <li><a href="#">Magazin</a></li>
                                            <li><a href="#">Denme</a></li>
                                            <li><a href="#">Yaz Haberi</a></li>
                                            <li><a href="#">Seyehat</a></li>
                                            <li><a href="#">Çeşitli Kategori</a></li>
                                            <li><a href="#">Başlıklar Buraya</a></li>
                                            <li><a href="#">Denem Burası</a></li>
                                        </ul>
                                    </div><!-- /.tumMenuler -->
                                </li>
                            </ul>
                        </nav>
                    </div><!-- /. tutucu -->
                </div><!-- /. menu -->
            </header>
            <div class="clearfix"></div>
            <section class="mansetHaberler">
                <div class="tutucu">
                    <div class="mansetHaber sol">
                        <div class="mansetGorsel anaMansetGorsel1">
                            <a href="#" class="onceki">Önceki</a>
                            <a href="#" class="buyukGorsel">
                                <img  alt="buyuk" target="_blank" src="assets/img/loader.gif" />
                            </a>
                            <a href="#" class="sonraki">Sonraki</a>
                        </div><!-- /. mansetGorsel -->
                        <div class="digerMansetler anaDigerMansetler">
                            <a href="#" data-buyuk-resim="assets/thumb/1b.jpg" data-title="Yazı Buraya gelecek 1" data-href="#1">
                                <img src="assets/thumb/1b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/2b.jpg" data-title="Yazı Buraya gelecek 2" data-href="#2">
                                <img src="assets/thumb/2b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/3b.jpg" data-title="Yazı Buraya gelecek 3" data-href="#3">
                                <img src="assets/thumb/3b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/4b.jpg" data-title="Yazı Buraya gelecek 4" data-href="#4">
                                <img src="assets/thumb/4b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/5b.jpg" data-title="Yazı Buraya gelecek 5" data-href="#5">
                                <img src="assets/thumb/5b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/6b.jpg" data-title="Yazı Buraya gelecek 6" data-href="#6">
                                <img src="assets/thumb/6b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/7b.jpg" data-title="Yazı Buraya gelecek 7" data-href="#7">
                                <img src="assets/thumb/7b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/8b.jpg" data-title="Yazı Buraya gelecek 8" data-href="#8">
                                <img src="assets/thumb/8b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/9b.jpg" data-title="Yazı Buraya gelecek 9" data-href="#9">
                                <img src="assets/thumb/9b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/10b.jpg" data-title="Yazı Buraya gelecek 10" data-href="#10">
                                <img src="assets/thumb/10b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/6b.jpg" data-title="Yazı Buraya gelecek 11" data-href="#11">
                                <img src="assets/thumb/6b.jpg" alt="1"/>
                            </a>
                            <a href="#" data-buyuk-resim="assets/thumb/3b.jpg" data-title="Yazı Buraya gelecek 12" data-href="#12">
                                <img src="assets/thumb/3b.jpg" alt="1"/>
                            </a>
                        </div><!-- /. digerMansetler -->
                    </div><!-- /. mansetHaber -->
                    <div class="yanSlider mansetHaber2 sag">
                        <div class="mansetGorsel yanMansetGorsel1">
                            <a href="#" class="onceki">Önceki</a>
                            <a href="#" class="buyukGorsel">
                                <img  alt="buyuk" target="_blank" src="assets/img/loader.gif" />
                            </a>
                            <a href="#" class="sonraki">Sonraki</a>
                        </div><!-- /. mansetGorsel -->
                        <div class="digerMansetler yanDigerMansetler">
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/1b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 2" data-href="#1" data-buyuk-resim="assets/thumb/2b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 3" data-href="#1" data-buyuk-resim="assets/thumb/3b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/4b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/5b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/6b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/7b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/8b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/9b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/10b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/1b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/2b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/3b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/4b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/5b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/6b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/7b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/8b.jpg"></a>
                            <a href="#" data-title="Yazı Buraya gelecek 1" data-href="#1" data-buyuk-resim="assets/thumb/9b.jpg"></a>
                        </div><!-- /. digerMansetler -->
                    </div><!-- /. yanSlider -->
                    <div class="yanMansetler sag">
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="assets/thumb/mac.jpg" alt="mac"/>
                                    <span>Bilyoner Olmak İster"le ortaya çıkan gerçek</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="assets/thumb/selcuk.jpg" alt="selcuk.jpg"/>
                                    <span>Olmaak İster ortaya çıkan gerçek</span>
                                </a>
                            </li>
                        </ul>
                    </div><!-- /. yanMansetler -->
                </div><!-- /. tutucu -->
            </section><!-- /. mansetHaberler -->
            <main class="orta">
                <div class="tutucu">
                    <section class="haberlerSol sol">
                        <div class="blok magazinHaberleri">
                            <h3 class="baslik"> <i class="fa fa-slideshare"></i> MAGAZİN</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/1.jpg" alt="1"/>
                                        <span>Mutluluk Kaynağı Çikolata Geçmişt</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/2.jpg" alt="1"/>
                                        <span>Fili iPhoane'la Besleyen Turistler Var</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /. magazin -->
                        <div class="haberler blok">
                            <h3 class="baslik"> <i class="fa fa-newspaper-o"></i> HABERLER</h3>
                            <ul class="haberListelemeUl">
                                <li class="ilkHaber">
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/4.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><h3>Meteoroloji'den Kar ve Fırtına Uyarısı</h3></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege. CHP Antalya Milletvekili Deniz Baykal, Bütçe görüşmelerinin 2'nci tur çalışmalarını izlemek için Genel Kurul salonuna geldi.</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i> 3</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/5.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Meteoroloji'den Kar ve Fırtına Uyarısı</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/6.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Pozantı Çocukları: İşkenceye Maruz Kalan Çocuklara Dava Erdoğan'dan Kılıçdaroğlunda</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/7.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Meteoroloji'den Kar ve Fırtına Uyarısı</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/8.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Pozantı Çocukları: İşkenceye Maruz Kalan Çocuklara Dava Erdoğan'dan Kılıçdaroğlunda</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/5.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Meteoroloji'den Kar ve Fırtına Uyarısı</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/6.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Pozantı Çocukları: İşkenceye Maruz Kalan Çocuklara Dava Erdoğan'dan Kılıçdaroğlunda</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/7.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Meteoroloji'den Kar ve Fırtına Uyarısı</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/8.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Pozantı Çocukları: İşkenceye Maruz Kalan Çocuklara Dava Erdoğan'dan Kılıçdaroğlunda</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/7.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Meteoroloji'den Kar ve Fırtına Uyarısı</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                                <li>
                                    <a href="#" class="haberGorsel">
                                        <img class="lazy" src="assets/img/blank.gif" data-original="assets/thumb/8.jpg" alt="1"/>
                                    </a>
                                    <div class="haberDetay">
                                        <a href="#"><strong>Pozantı Çocukları: İşkenceye Maruz Kalan Çocuklara Dava Erdoğan'dan Kılıçdaroğlunda</strong></a>
                                        <p>Meteoroloji İç Anadolu, Ege ve Akdeniz bölgelerinin yüksek kesimlerinde kar yağışı; Marmara, Güney Ege...</p>
                                        <time class="tarih">25 Kasım 2014</time>
                                        <span class="yorum"><i class="fa fa-comment-o"></i>0</span>
                                    </div><!-- /. haberDetay -->
                                </li>
                            </ul>
                        </div><!-- /. haberler -->
                    </section><!-- /. haberlerSol -->
                    <aside class="haberlerSag sag">
                        <div class="teknolojiBolumu blok">
                            <h3 class="baslik">
                                <i class="fa fa-cog"></i> TEKNOLOJİ
                                <span class="ileriGeri">
                                    <a href="#" class="geri"><i class="fa fa-chevron-left"></i></a>
                                    <a href="#" class="ileri"><i class="fa fa-chevron-right"></i></a>
                                </span>
                            </h3>
                            <div id="teknolojiSlider" class="owl-carousel">
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Vğar Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                                <div class="item">
                                    <div class="slideIcerik">
                                        <a href="#">Fili iPhone"la Besleyen Turistler Var</a>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam efficitur aliquam pulvi.</p>
                                    </div><!-- /. slideIcerik -->
                                    <div class="sliderGorsel">
                                        <div class="dikeyTable">
                                            <a href="#" class="dikeyTableCell">
                                                <img class="lazyOwl" data-src="assets/thumb/saat.jpg"  alt="saat"/>
                                            </a>
                                        </div><!-- /. dikeyOrtalama -->
                                    </div><!-- /. sliderGorsel -->
                                </div><!-- /. item -->
                            </div><!-- /. owl-carousel -->
                        </div><!-- /. teknolojiSlider -->
                        <div class="fotoVideoBlok blok">
                            <h3 class="baslik"> <i class="fa fa-picture-o"></i> FOTO GALERİ</h3>
                            <ul>
                                <li class="buyuk">
                                    <a href="#">
                                        <img src="assets/thumb/s1.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Kore'de Herkesi Şaşkına Çeviren Yol Kavga 
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-picture-o"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="sagAlan">
                                    <a href="#">
                                        <img src="assets/thumb/s5.jpg" alt="s5"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/s2.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Kore'de Herkesi Şaşkına Çeviren Yol Kavga 
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-picture-o"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/s3.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Kore'de Herkesi Şaşkına Çeviren Yol Kavga 
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-picture-o"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img  src="assets/thumb/s4.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Kore'de Herkesi Şaşkına Çeviren Yol Kavga 
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-picture-o"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div><!-- /. fotoVideoBlok -->
                        <div class="fotoVideoBlok videoGaleri blok">
                            <h3 class="baslik"> <i class="fa fa-video-camera"></i> GÜNÜN VİDEOLARI</h3>
                            <ul>
                                <li class="buyuk">
                                    <a href="#">
                                        <img src="assets/thumb/s6.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Metallica'nın Nurella ile İmtihanı
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </a>
                                </li>
                                <li class="sagAlan">
                                    <a href="#">
                                        <img src="assets/thumb/s10.jpg" alt="s5"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/s7.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Kore'de Herkesi Şaşkına Çeviren Yol Kavga 
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/s8.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Kore'de Herkesi Şaşkına Çeviren Yol Kavga 
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/s9.jpg" alt="s5"/>
                                        <span class="baslik">
                                            Kore'de Herkesi Şaşkına Çeviren Yol Kavga 
                                            <span>
                                                <i class="fa fa-users"></i>
                                                1.213
                                            </span>
                                        </span>
                                        <span class="resimIkon">
                                            <i class="fa fa-play"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div><!-- /. fotoVideoBlok -->
                    </aside><!-- /. haberlerSag -->
                    <div class="clearfix"></div>
                    <section class="sporHaberleri blok">
                        <h3 class="baslik"><i class="fa fa-soccer-ball-o"></i>SPOR </h3>
                        <div class="ileriGeri">
                            <a href="#" class="geri">Geri</a>
                            <a href="#" class="ileri">İleri</a>
                        </div>
                        <div id="sporSlider" class="owl-carousel" >
                            <div class="item">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/arda.jpg" alt="arda" />
                                            <span>Mutluluk Kaynağı Çikolata Geçmişt</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/fb.jpg" alt="fb"/>
                                            <span>Fili iPhone'la Besleyen Turistler Var</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/arda2.jpg" alt="arda2"/>
                                            <span>Sağlam için flaş iddia!</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/tugay.jpg" alt="tugay"/>
                                            <span>Fili iPhone'la Besleyen Turistler Var</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/ts.jpg" alt="ts"/>
                                            <span>Sağlam için flaş iddia!</span>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /. item -->
                            <div class="item">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/arda.jpg" alt="arda" />
                                            <span>Mutluluk Kaynağı Çikolata Geçmişt</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/fb.jpg" alt="fb"/>
                                            <span>Fili iPhone'la Besleyen Turistler Var</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/arda2.jpg" alt="arda2"/>
                                            <span>Sağlam için flaş iddia!</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/tugay.jpg" alt="tugay"/>
                                            <span>Fili iPhone'la Besleyen Turistler Var</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  class="lazyOwl" src="assets/thumb/ts.jpg" alt="ts"/>
                                            <span>Sağlam için flaş iddia!</span>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /. item -->
                        </div><!-- /. owl-carousel -->
                    </section><!-- /. sporHaberleri -->
                    <section class="yerelHaberlerGazeteler blok">
                        <div class="yerelHaberler sol">
                            <h3 class="baslik"><i class="fa fa-cube"></i> YEREL HABERLER</h3>
                            <div class="yerelHaberler">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/1.jpg" alt="1"/>
                                            <span>Mutluluk Kaynağı Çikolata Geçmişt</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/2.jpg" alt="1"/>
                                            <span>Meteorolojiden 'Çok Kuvvetli' Uyarı</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/3.jpg" alt="1"/>
                                            <span>Meclis'te Atatürk'ün 'Harf Devrimi'ne Hakaret</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/4.jpg" alt="1"/>
                                            <span>Herkes Köpek Gibi Koştu</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/5.jpg" alt="1"/>
                                            <span>Her Kadının En Az Bir Kere Yaşadığı 10 Maskara Facia</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/6.jpg" alt="1"/>
                                            <span>İsrail Suriye'yi Vurdu</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/7.jpg" alt="1"/>
                                            <span>Kartal Konya'da Kanatlandı</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/8.jpg" alt="1"/>
                                            <span>Otomobiller Acil Durumda Çağrı Yapacak</span>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /. yerelHaberler -->
                        </div><!-- /. yerelHaberler -->
                        <div class="gazeteMansetleri sag">
                            <h3 class="baslik center">GAZETE MANŞETLERİ</h3>
                            <div class="gazetelerSlider">

                                <div class="ms-caro3d-template ms-caro3d-wave">
                                    <div class="master-slider ms-skin-default" id="masterslider">
                                        <div class="ms-slide">
                                            <img src="assets/img/loading.gif" data-src="assets/thumb/gazetelerSlider/gazete1.jpg" alt="lorem ipsum dolor sit"/> 
                                        </div>
                                        <div class="ms-slide">
                                            <img src="assets/img/loading.gif" data-src="assets/thumb/gazetelerSlider/gazete2.jpg" alt="lorem ipsum dolor sit"/>    
                                        </div>
                                        <div class="ms-slide">
                                            <img src="assets/img/loading.gif" data-src="assets/thumb/gazetelerSlider/gazete3.jpg" alt="lorem ipsum dolor sit"/>   
                                        </div>
                                        <div class="ms-slide">
                                            <img src="assets/img/loading.gif" data-src="assets/thumb/gazetelerSlider/gazete1.jpg" alt="lorem ipsum dolor sit"/>      
                                        </div>
                                        <div class="ms-slide">
                                            <img src="assets/img/loading.gif" data-src="assets/thumb/gazetelerSlider/gazete2.jpg" alt="lorem ipsum dolor sit"/> 
                                        </div>
                                        <div class="ms-slide">
                                            <img src="assets/img/loading.gif" data-src="assets/thumb/gazetelerSlider/gazete3.jpg" alt="lorem ipsum dolor sit"/>       
                                        </div>
                                        <div class="ms-slide">
                                            <img src="assets/img/loading.gif" data-src="assets/thumb/gazetelerSlider/gazete1.jpg" alt="lorem ipsum dolor sit"/> 
                                        </div>
                                    </div><!-- /. master-slider .ms-skin-default #masterslider -->
                                </div><!-- /. ms-caro3d-template .ms-caro3d-wave -->

                                <div id="gazetelerSliderYon"></div>
                                <a class="altBaslik" href="#">Tüm Manşetler</a>
                            </div><!-- /. gazetelerSlider -->
                        </div><!-- /. gazeteMansetleri -->
                    </section><!-- /. yerelHaberlerGazeteler -->
                    <div class="clearfix"></div>
                    <section class="gundemdekiKisiler blok">
                        <h3 class="baslik">
                        <i class="fa fa-users"></i>
                        GÜNDEMDEKİ KİŞİLER 
                        <span class="top20">(TOP 20)</span> 
                            <span class="ileriGeri">
                                <a href="#" class="geri"><i class="fa fa-chevron-left"></i></a>
                                <a href="#" class="ileri"><i class="fa fa-chevron-right"></i></a>
                            </span>
                        </h3>
                        <div id="gundemdekiKisilerSlider" class="owl-carousel" >
                            <div class="item">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/my.jpg" alt="arda" />
                                            <span>Mesut Yar Hatay Biberi</span>
                                            <span class="popularite">1</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/mk.jpg" alt="fb"/>
                                            <span>Melis Kar</span>
                                            <span class="popularite">2</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/eg.jpg" alt="arda2"/>
                                            <span>Ebru Gündeş</span>
                                            <span class="popularite">3</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/h.jpg" alt="tugay"/>
                                            <span>Hadise</span>
                                            <span class="popularite">4</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/dk.jpg" alt="ts"/>
                                            <span>Demek Akalın</span>
                                            <span class="popularite">5</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/eg.jpg" alt="arda2"/>
                                            <span>Ebru Gündeş</span>
                                            <span class="popularite">6</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/h.jpg" alt="tugay"/>
                                            <span>Hadise</span>
                                            <span class="popularite">7</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/dk.jpg" alt="ts"/>
                                            <span>Demek Akalın</span>
                                            <span class="popularite">8</span>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /. item -->
                            <div class="item">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/my.jpg" alt="arda" />
                                            <span>Mesut Yar</span>
                                            <span class="popularite">1</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/mk.jpg" alt="fb"/>
                                            <span>Melis Kar</span>
                                            <span class="popularite">2</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/eg.jpg" alt="arda2"/>
                                            <span>Ebru Gündeş</span>
                                            <span class="popularite">3</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/h.jpg" alt="tugay"/>
                                            <span>Hadise</span>
                                            <span class="popularite">4</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/dk.jpg" alt="ts"/>
                                            <span>Demek Akalın</span>
                                            <span class="popularite">5</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/eg.jpg" alt="arda2"/>
                                            <span>Ebru Gündeş</span>
                                            <span class="popularite">6</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img  src="assets/thumb/h.jpg" alt="tugay"/>
                                            <span>Hadise</span>
                                            <span class="popularite">7</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="assets/thumb/dk.jpg" alt="ts"/>
                                            <span>Demek Akalın </span>
                                            <span class="popularite">8</span>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /. item -->
                        </div><!-- /. owl-carousel -->
                    </section><!-- /. gundemdekiKisiler -->
                </div><!-- /. tutucu -->
                <div class="clearfix"></div>
            </main>
            <footer class="footer yerelFooter">
                <div class="footer2Tutucu">
                    <div class="tutucu">
                        <div class="bolum1">
                            <div class="baslikBolum">
                                <a class="logo" href="#"><img src="assets/img/logo2.png" alt="eHaber"/></a>
                            </div><!-- /. baslikBolum -->
                            <div class="bolum1Menu">
                                <a href="#"><i class="fa fa-exclamation-triangle"></i>Son Dakika</a>
                                <a href="#"><i class="fa fa-clock-o"></i>Gündem</a>
                                <a href="#"><i class="fa fa-star-o"></i>Manşetler</a>
                                <a href="#"><i class="fa fa-video-camera"></i>Video Galeri</a>
                                <a href="#"><i class="fa fa-image"></i>Foto Galeri</a> 
                                <a href="#"><i class="fa fa-futbol-o"></i>Spor</a>
                                <a href="#"><i class="fa fa-briefcase"></i>Finans</a>
                            </div><!-- /. bolum1Menu -->
                            <div class="sosyaAglar">
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#"><i class="fa fa-send"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </div><!-- /. sosyalAglar -->
                        </div><!-- /. bolum1 -->
                        <div class="bolum2">
                            <div class="baslikBolum">
                                <span class="bolumBaslik">Gazeteler</span>
                            </div><!-- /. baslikBolum -->
                            <div class="gazetelerListe">
                                <ul>
                                    <li><a href="#">Akşam</a></li>
                                    <li><a href="#">Anayurt</a></li>
                                    <li><a href="#">Aydınlık</a></li>
                                    <li><a href="#">Birgün</a></li>
                                    <li><a href="#">Bugün</a></li>
                                    <li><a href="#">Cumhuriyet</a></li>
                                    <li><a href="#">Dünya</a></li>
                                    <li><a href="#">Evrensel</a></li>
                                    <li><a href="#">Fanatik</a></li>
                                    <li><a href="#">Fotomaç</a></li>
                                    <li><a href="#">Güneş</a></li>
                                    <li><a href="#">HaberTurk</a></li>
                                    <li><a href="#">Hürriyet</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Milat</a></li>
                                    <li><a href="#">Milli Gazete</a></li>
                                    <li><a href="#">Milliyet</a></li>
                                    <li><a href="#">Ortadoğu</a></li>
                                    <li><a href="#">Özgür Gündem</a></li>
                                    <li><a href="#">Posta</a></li>
                                    <li><a href="#">Sabah</a></li>
                                    <li><a href="#">Sözcü</a></li>
                                    <li><a href="#">Star</a></li>
                                    <li><a href="#">Takvim</a></li>
                                    <li><a href="#">Taraf</a></li>
                                    <li><a href="#">Türkiye</a></li>
                                    <li><a href="#">Vatan</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Yeni Akit</a></li>
                                    <li><a href="#">Yeni Asya</a></li>
                                    <li><a href="#">Yeni Çağ</a></li>
                                    <li><a href="#">Yeni Mesaj</a></li>
                                    <li><a href="#">Yeni Şafak</a></li>
                                    <li><a href="#">Yurt</a></li>
                                    <li><a href="#">Zaman</a></li>
                                </ul>
                                <div class="clearfix"></div>
                                <a href="#" class="siteneHaberEkle"><i class="fa fa-plus-square"></i> Sitene Haber Ekle</a>
                            </div><!-- /. gazetelerListe -->
                        </div><!-- /. bolum2 -->
                        <div class="bolum3">
                            <div class="baslikBolum">
                                <span class="bolumBaslik">Yerel Haber</span>
                            </div><!-- /. baslikBolum -->
                            <div class="yerelHaberlerListe">
                                <ul>
                                    <li><a href="#">Adana</a></li>
                                    <li><a href="#">Adıyaman</a></li>
                                    <li><a href="#">Afyon</a></li>
                                    <li><a href="#">Ağrı</a></li>
                                    <li><a href="#">Aksaray</a></li>
                                    <li><a href="#">Amasya</a></li>
                                    <li><a href="#">Ankara</a></li>
                                    <li><a href="#">Antalya</a></li>
                                    <li><a href="#">Ardahan</a></li>
                                    <li><a href="#">Artvin</a></li>
                                    <li><a href="#">Aydın</a></li>
                                    <li><a href="#">Balıkesir</a></li>
                                    <li><a href="#">Bartın</a></li>
                                    <li><a href="#">Batman</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Bayburt</a></li>
                                    <li><a href="#">Bilecik</a></li>
                                    <li><a href="#">Bingöl</a></li>
                                    <li><a href="#">Bitlis</a></li>
                                    <li><a href="#">Bolu</a></li>
                                    <li><a href="#">Burdur</a></li>
                                    <li><a href="#">Bursa</a></li>
                                    <li><a href="#">Çanakkale</a></li>
                                    <li><a href="#">Çankırı</a></li>
                                    <li><a href="#">Çorum</a></li>
                                    <li><a href="#">Denizli</a></li>
                                    <li><a href="#">Diyarbakır</a></li>
                                    <li><a href="#">Düzce</a></li>
                                    <li><a href="#">Edirne</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Elazığ</a></li>
                                    <li><a href="#">Erzincan</a></li>
                                    <li><a href="#">Erzurum</a></li>
                                    <li><a href="#">Eskişehir</a></li>
                                    <li><a href="#">Gaziantep</a></li>
                                    <li><a href="#">Giresun</a></li>
                                    <li><a href="#">Gümüşhane</a></li>
                                    <li><a href="#">Hakkari</a></li>
                                    <li><a href="#">Hatay</a></li>
                                    <li><a href="#">Iğdır</a></li>
                                    <li><a href="#">Isparta</a></li>
                                    <li><a href="#">İstanbul</a></li>
                                    <li><a href="#">İzmir</a></li>
                                    <li><a href="#">K.Maraş</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Karabük</a></li>
                                    <li><a href="#">Karaman</a></li>
                                    <li><a href="#">Kars</a></li>
                                    <li><a href="#">Kastamonu</a></li>
                                    <li><a href="#">Kayseri</a></li>
                                    <li><a href="#">Kırıkkale</a></li>
                                    <li><a href="#">Kırklareli</a></li>
                                    <li><a href="#">Kırşehir</a></li>
                                    <li><a href="#">Kilis</a></li>
                                    <li><a href="#">Kocaeli</a></li>
                                    <li><a href="#">Konya</a></li>
                                    <li><a href="#">Kütahya</a></li>
                                    <li><a href="#">Malatya</a></li>
                                    <li><a href="#">Manisa</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Mardin</a></li>
                                    <li><a href="#">Mersin</a></li>
                                    <li><a href="#">Muğla</a></li>
                                    <li><a href="#">Muş</a></li>
                                    <li><a href="#">Nevşehir</a></li>
                                    <li><a href="#">Niğde</a></li>
                                    <li><a href="#">Ordu</a></li>
                                    <li><a href="#">Osmaniye</a></li>
                                    <li><a href="#">Rize</a></li>
                                    <li><a href="#">Sakarya</a></li>
                                    <li><a href="#">Samsun</a></li>
                                    <li><a href="#">Siirt</a></li>
                                    <li><a href="#">Sinop</a></li>
                                    <li><a href="#">Sivas</a></li>
                                </ul>
                                <ul>
                                    <li><a href="#">Şanlıurfa</a></li>
                                    <li><a href="#">Şırnak</a></li>
                                    <li><a href="#">Tekirdağ</a></li>
                                    <li><a href="#">Tokat</a></li>
                                    <li><a href="#">Trabzon</a></li>
                                    <li><a href="#">Tunceli</a></li>
                                    <li><a href="#">Uşak</a></li>
                                    <li><a href="#">Van</a></li>
                                    <li><a href="#">Yalova</a></li>
                                    <li><a href="#">Yozgat</a></li>
                                    <li><a href="#">Zonguldak</a></li>
                                    <li><a href="#">Tüm İller</a></li>
                                </ul>
                                <div class="uygulamaIconlar">
                                    <a href="#" class="mu ios"></a>
                                    <a href="#" class="mu android"></a>
                                    <a href="#" class="mu windows"></a>
                                    <a href="#" class="mu blackberry"></a>
                                </div><!-- /. uygulamaIconlar -->
                                <div class="clearfix"></div>
                            </div><!-- /. gazetelerListe -->
                        </div><!-- /. bolum3 -->
                    </div><!-- /. tutucu -->
                    <div class="clearfix"></div>
                </div><!-- /. footerTutucu -->
                <div class="footerBulten">
                    <div class="tutucu">
                        <div class="bulten">
                            <input type="text" placeholder="Haber e-Bülten'e üye olun!"/>
                            <input type="submit" value="Kayıt Ol"/>
                        </div>
                        <div class="siteHakkinda">
                            <a href="#">ANA SAYFA</a>
                            <a href="#">HABERLER</a>
                            <a href="#">HAKKIMIZDA</a>
                            <a href="#">GİZLİLİK</a>
                            <a href="#">İLETİŞİM</a>
                        </div><!-- /. siteHakkinda -->
                    </div><!-- /. tutucu -->
                </div><!-- /. footerBulten -->
                <div class="footerDoruk">
                    <div class="tutucu">
                        <p class="sol">
                            <a href="#" class="sprite doruk"></a>
                            Copyright 2015 Doruk Dijital Medya Yazılım A.Ş
                        </p>
                        <div class="uyelikler">
                            <span class="aa sprite">AA</span>
                            <span class="iha sprite">iha</span>
                            <span class="cihan sprite">cihan</span>
                            <span class="dha sprite">dha</span>
                        </div><!-- /. uyelikler -->
                    </div><!-- /. tutucu -->
                </div><!-- /. footerDoruk -->
                <a data-target=".cerceve" class="yukariZipla">YUKARI ÇIK<i class="fa fa-chevron-up" aria-hidden="true"></i></a>
            </footer><!-- /. footer -->
        </div><!-- /. cerceve -->
                <div id='resimYukle' class="standartPopGorunumu simplePop">
            <div class="baslik">
                <h4><i class="fa fa-picture-o"></i> Resim Ekleme İşlemi</h4>
                <a href="#" class="popKapat1" ><i class="fa fa-times"></i></a>
            </div><!-- /. baslik -->
            <div class="detay">
                <div class="resimYukleCerceve">
                    <form action="/file-upload" class="dropzone dropzone1">
                        <div class="fallback">
                            <input id="resimYukleme" name="file" type="file" accept="image/png" />
                        </div><!-- /. fallback -->
                        <div class="dz-message" data-dz-message><span><i class="fa fa-camera"></i>Tıklayıp bir resim seçin veya<strong>Resimi sürükleyin ve buraya bırakın</strong></span></div>
                    </form>
                </div><!-- /. resimYukleCerceve -->
            </div><!-- /. detay -->
        </div><!-- /. resimYukle -->
        <div id='yorumGonderildi' class="simplePop">
            <div class="detay">
                <i class="fa fa-check"></i>
                <h5>Yorumunuz Gönderildi</h5>
                <p>Yazmış olduğunuz yorum editörlerimiz tarafından onaylandıktan sonra sitede yayınlanacaktır.Daha hızlı yorum yapabilmek için ve sitemizin diğer özelliklerinden yararlanabilmek için <a class="popUyeOlButon" href="#">Üye Ol</a>’un!</p>
            </div><!-- /. detay -->
            <div class="birDahaGosterme">
                <label><input class="abCheckbox" type="checkbox" /> Bu uyarıyı bir daha gösterme</label>
                <a href="#" class="popKapat2" >TAMAM</a>
            </div><!-- /. birDahaGosterme -->
        </div><!-- /. yorumGonderildi -->

        <div id='videoEklePop' class="standartPopGorunumu simplePop">
            <div class="baslik">
                <h4><i class="fa fa-video-camera"></i> Video Ekleme İşlemi</h4>
                <a href="#" class="popKapat3" ><i class="fa fa-times"></i></a>
            </div><!-- /. baslik -->
            <div class="detay">
                <div class="videoEkleCerceve">
                    <p>
                        Değerli okurumuz,
                        <br/>
                        <br/>
                        Ehaber’deki içeriklere yorum yaparken, yorumunuza video ekleyebilirsiniz. Video ekleme işlemini sadece aşağıdaki siteler aracılığıyla yapabilirsiniz.
                    </p>
                    <div class="videoEkle">
                        <span class="kucukBaslik yesil">Video Linki</span>
                        <input id="videoLink" type="text" placeholder="örn: https://www.youtube.com/watch?v=OTzJtfXL56Q">
                        <input id="videoGonder" type="submit" value="Gönder" />
                    </div><!-- /. videoEkle -->
                </div><!-- /. videoEkleCerceve -->
            </div><!-- /. detay -->
        </div><!-- /. videoEklePop -->

        <div id='videoEklePop2' class="standartPopGorunumu simplePop">
            <div class="baslik">
                <h4><i class="fa fa-video-camera"></i> Video Ekleme İşlemi</h4>
                <a href="#" class="popKapat4" ><i class="fa fa-times"></i></a>
            </div><!-- /. baslik -->
            <div class="detay">
                <div class="videoEkleCerceve">
                    <p>
                        Değerli okurumuz,
                        <br/>
                        <br/>
                        Ehaber’deki içeriklere yorum yaparken, yorumunuza video ekleyebilirsiniz. Video ekleme işlemini sadece aşağıdaki siteler aracılığıyla yapabilirsiniz.
                    </p>
                    <div class="videoEkle">
                        <span class="kucukBaslik kirmizi"><i class="fa fa-times"></i> Gönderdiğniz video izin verilen siteler arasında yoktur.</span>
                        <span class="hata"><strong>İzin verilen siteler</strong>Ehaber.com, Youtube.com, İzlesene.com, Dailymotion.com, Web.tv, Vimeo.com</span>
                        <a class="tekrarDene" href="#">Tekrar Dene</a>
                    </div><!-- /. videoEkle -->
                </div><!-- /. videoEkleCerceve -->
            </div><!-- /. detay -->
        </div><!-- /. videoEklePop2 -->

        <div id='uyeBildir' class="standartPopGorunumu simplePop">
            <div class="baslik">
                <h4>Üyeyi Bildir</h4>
                <a href="#" class="popKapat5" ><i class="fa fa-times"></i></a>
            </div><!-- /. baslik -->
            <div class="detay">
                <div class="uyeBildirTututucu">
                    <div class="profile d1">
                        <div class="d2">
                            <img src="assets/thumb/bildirilenUye.jpg" alt="bildirilenUye"/>
                        </div><!-- /. d2 -->
                        <div class="d2">
                            <span>Faruk Atasoy</span>
                        </div><!-- /. d2 -->
                    </div><!-- /. profile -->
                    <textarea placeholder="Üye ile ilgili şikayetinizi yazınız..."></textarea>
                    <input class="gonder" value="Gönder" type="submit" />
                </div><!-- /. uyeBildirTututucu -->
                <div class="uyeBildirildi">
                    <i class="fa fa-check"></i>
                    <p>Üye ile ilgili şikayetiniz bize ulaştı. En kısa zamanda editörülerimiz<br/> tarafından değerlendirilip, size bilgi verilecektir.</p>
                </div><!-- /. uyeBildirildi -->
            </div><!-- /. detay -->
        </div><!-- /. uyeBildir -->
        <div id ="mesaj" class="simplePop mesajPopTutucu">
            <div class="mesajicerik">
                <div class="kullaniciAdi mesajPopup">
                    <img src="assets/thumb/profilResmi3.png" alt="profilResmi"/>
                    <span>İlker Değirmencioğlu</span>
                    <a class="mesajPopKapat" href="#"><i class="fa fa-times"></i></a>
                </div><!-- /. kullaniciAdi -->
                <input type="hidden" id="user1" value="assets/thumb/profilResmi3.png"/>
                <input type="hidden" id="user2" value="assets/thumb/profilResmi2.png"/>
                <input type="hidden" id="time" value="19:35"/>
                <div class="mesajAlani mesajPopup">
                    <div id='resimYukle2' class="width">
                        <div class="detay">
                            <div class="resimYukleCerceve">
                                <form action="/file-upload" class="dropzone dropzone2">
                                    <div class="fallback">
                                        <input id="resimYukleme" name="file" type="file" accept="image/png" />
                                    </div><!-- /. fallback -->
                                    <div class="dz-message" data-dz-message><span><i class="fa fa-camera"></i>Tıklayıp bir resim seçin veya<strong>Resimi sürükleyin ve buraya bırakın</strong></span></div>
                                </form>
                            </div><!-- /. resimYukleCerceve -->
                        </div><!-- /. detay -->
                    </div><!-- /. resimYukle2 -->
                    <div class="nano">
                        <div class="nano-content">
                            <ul>
                                <li  class="mesajYok">
                                    <p>Henüz hiç konuşmamıssınız...</p>
                                </li>
                                <li class="mesaj kullanici1">
                                    <a href="#"><img class="profil" src="assets/thumb/profilResmi3.png" alt="profilResmi"/></a>
                                    <p>
                                        What is asd? :-)
                                        <span>19.05</span>
                                    </p>
                                </li><!-- /. kullanici1 -->
                                <li class="mesaj kullanici2">
                                    <a href="#"><img class="profil" src="assets/thumb/profilResmi2.png" alt="profilResmi"/></a>
                                    <p>
                                        ya ilk defa kullandım burdan mesaji en yakında sen vardın bir deneyim dedim :)
                                        <span>19.05</span>
                                    </p>
                                </li><!-- /. kullanici2 -->
                                <li class="mesaj kullanici1">
                                    <a href="#"><img class="profil" src="assets/thumb/profilResmi3.png" alt="profilResmi"/></a>
                                    <p> Hah.d peki bkalm
                                        <span>19.05</span>
                                    </p>
                                </li><!-- /. kullanici1 -->
                                <li class="mesaj kullanici1">
                                    <a href="#"><img class="profil" src="assets/thumb/profilResmi3.png" alt="profilResmi"/></a>
                                    <p>
                                        eywalllahhhh :d tşk
                                        <span>19.05</span>
                                    </p>
                                </li><!-- /. kullanici1 -->
                            </ul>
                        </div><!-- /. nano-content -->
                    </div><!-- /. nano -->
                </div><!-- /. mesajAlani -->
                <div class="mesajGonder">
                    <div class="dosyaEkle">
                        <div class="imgWrapper"></div>
                    </div><!-- /. dosyaEkle -->
                    <div class="contentWrapper">
                        <input class="mesajYazPopup" type="text" placeholder="Mesaj yaz..."/>
                        <div class="ekle">
                            <div class="tooltipMesaj">
                                <a href="javascript:void(0)"><i class="fa fa-paperclip" aria-hidden="false"></i></a>
                                <div class="mesajtooltip">
                                    <span><i></i>Mesajlarınıza dilerseniz resim veya video ekleyebilirsiniz.</span>
                                </div><!-- /. tooltiptext -->
                            </div><!-- /. tooltip -->
                        </div><!-- /. ekle -->
                        <div class="content">
                            <a class="gonder" href="javascript:void(0)">GÖNDER</a>
                            <a href="javascript:void(0)" class="smile"><i class="fa fa-smile-o" aria-hidden="true"></i></a>
                            <ul class="emoji">
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile1.png" data-code=":D"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile2.png" alt="..." data-code=":A"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile3.png" alt="..." data-code=":B"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile4.png" alt="..." data-code=":C"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile5.png" alt="..." data-code=":D"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile6.png" alt="..."  data-code=":E"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile7.png" alt="..."  data-code=":F"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile8.png" alt="..." data-code=":G"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile9.png" alt="..." data-code=":H"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile10.png" alt="..." data-code=":K"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile11.png" alt="..." data-code=":L"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile12.png" alt="..." data-code=":M"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile13.png" alt="..." data-code=":N"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile14.png" alt="..." data-code=":O"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile15.png" alt="..." data-code=":P"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile16.png" alt="..." data-code=":R"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile17.png" alt="..." data-code=":S"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile18.png" alt="..." data-code=":T"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile19.png" alt="..." data-code=":V"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile20.png" alt="..." data-code=":Y"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile21.png" alt="..." data-code=":Z"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile22.png" alt="..."/ data-code=":a">  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile23.png" alt="..." data-code=":b"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile24.png" alt="..." data-code=":c"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile25.png" alt="..." data-code=":d"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile26.png" alt="..." data-code=":e"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile27.png" alt="..." data-code=":f"/>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="assets/thumb/smile/smile28.png" alt="..." data-code=":g"/>  
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /. content -->
                    </div>
                </div><!-- /. mesajGonder -->
            </div><!-- /. mesajPopup -->
        </div><!-- /. mesaj -->
        <div id='girisYap' class="simplePop">
            <div class="girisPopup">
                <div class="popSol">
                    <div class="girisBolumu acik">
                        <i class="fa fa-user"></i>
                        <input type="text" placeholder="E-posta" />
                        <input type="password" placeholder="Şifre" />
                        <input type="submit" value="Giriş Yap" />
                        <div class="beniHatirla">
                            <label><input class="abCheckbox" type="checkbox" />Beni Hatırla</label>
                            <a class="sifreUnuttum" href="#">Şifremi Unuttum</a>
                        </div><!-- /. beniHatirla -->
                    </div><!-- /. girisBolumu -->

                    <div class="uyeOlBolumu">
                        <span class="baslik">Yeni Hesap Oluşturun</span>
                        <input class="isim" type="text" placeholder="Ad ve Soyad" />
                        <input class="eposta" type="text" placeholder="E-posta" />
                        <input class="sifre" type="password" placeholder="Şifre" />
                        <input class="sifreTekrar" type="password" placeholder="Şifre Tekrar" />
                        <div class="sozlesme">
                            <label><input class="sozlesmeCheckbox abCheckbox" type="checkbox" /><a href="#">Kullanıcı Sözleşmesini</a> kabul ediyorum.</label>
                        </div><!-- /. beniHatirla -->
                        <input type="submit" value="Üye ol" />
                    </div><!-- /. uyeOlBolumu -->

                    <div class="sifreSifirla">
                        <i class="fa fa-user"></i>
                        <span class="aciklama">Lütfen sisteme üye olduğunuz e-posta <br/> adresini giriniz.</span>
                        <input type="text" placeholder="E-posta" />
                        <input type="submit" value="Şifremi Sıfırla" />
                    </div><!-- /. sifreSifirla -->

                    <div class="sifrenizGonderildi">
                        <i class="fa fa-check"></i>
                        <span class="aciklama">Yeni şifreniz belirttiğiniz e-posta adresinize yollanmıştır. Yeni şifreniz ile giriş yaptıktan sonra şifrenizi değiştirmenizi öneriyoruz. </span>
                    </div><!-- /. sifrenizGonderildi -->

                    <div class="hataliEposta">
                        <i class="fa fa-times"></i>
                        <span class="aciklama">Girmiş olduğunuz e-posta sistemde bulunmamaktadır. Lütfen tekrar deneyin.</span>
                        <a class="tekrarDene" href="#">Tekrar Dene</a>
                    </div><!-- /. hataliEposta -->

                </div><!-- /. popSol -->
                <div class="popSag">
                    <a class="uyeGirisiPopKapat" href="#"><i class="fa fa-times"></i></a>
                    <div class="sosyalGirisBolumu">
                        <span>Sosyal ağ hesaplarınızla giriş yapın.</span>
                        <a class="facebook" href="#"><i class="fa fa-facebook"></i> Facebook ile tek tuşla bağlan</a>
                        <a class="twitter" href="#"><i class="fa fa-twitter"></i> Twitter ile tek tuşla bağlan</a>
                        <a class="googlePlus" href="#"><i class="fa fa-google-plus"></i> Google  + ile tek tuşla bağlan</a>
                    </div><!-- /. sosyalGirisBolumu -->
                    <span>veya</span>
                    <a class="uyelikOlusturButon" href="#">Yeni bir üyelik oluşturun</a>
                </div><!-- /. popSag -->
            </div><!-- /. girisPopup -->
        </div><!-- /. girisYap -->

        <div id='uyeOlAdim2' class="simplePop">
            <div class="kullaniciAdi">
                <span class="ad">Merhaba Burcu,</span>
                <span class="aciklama">Üyeliğinin tamamlanmasına sadece 1 adım kaldı <a href="#">Bu Adımı Atla <i class="fa fa-angle-double-right"></i></a></span>
            </div><!-- /. kullaniciAdi -->
            <div class="resimYuklemeAlani">
                <div class="yuklenenResim">
                    <img class="gelenResim" src="assets/img/yuklenmemisResim.jpg" alt="yuklenmemisResim"/>
                </div><!-- /. yuklenenResim -->
                <div class="resimSec">
                    <span>
                        <div class="fotografSecTutucu" ><input class="file" type="file" name="pic[]" accept="image/*" /><a class="fotografSec" href="#">Fotoğraf Yükle</a></div> veya <a class="avatarSec" href="#">Avatar Seç</a>
                        <br/>
                        Yüklediğiniz fotoğrafın boyutu maksimum 500x500 px ve 500 kb  boyutlarında olmalıdır.
                    </span>
                </div><!-- /. resimSec -->
                <div class="clearfix"></div>
                <div class="digerBilgiler">
                    <div class="solBolum">
                        <div class="takvim2">
                            <i class="fa fa-calendar"></i>
                            <input class="takvimAc2" type="text" placeholder="Doğum Tarihiniz">
                        </div><!-- /. takvim2 -->
                        <select class="select2">
                            <option>Yaşadığınız Yer</option>
                            <option>Yaşadığınız Yer 1</option>
                            <option>Yaşadığınız Yer 2</option>
                            <option>Yaşadığınız Yer 3</option>
                            <option>Yaşadığınız Yer 4</option>
                            <option>Yaşadığınız Yer 5</option>
                        </select>
                        <select class="select2">
                            <option>Cinsiyetiniz</option>
                            <option>Cinsiyetiniz 1</option>
                            <option>Cinsiyetiniz 2</option>
                            <option>Cinsiyetiniz 3</option>
                            <option>Cinsiyetiniz 4</option>
                            <option>Cinsiyetiniz 5</option>
                        </select>
                        <select class="select2">
                            <option>Mesleğiniz</option>
                            <option>Mesleğiniz 1</option>
                            <option>Mesleğiniz 2</option>
                            <option>Mesleğiniz 3</option>
                            <option>Mesleğiniz 4</option>
                            <option>Mesleğiniz 5</option>
                        </select>
                    </div><!-- /. solBolum -->
                    <div class="sagBolum">
                        <textarea placeholder="Kısaca Kendinizden bahsedin..."></textarea>
                        <input class="uyeligiTamamla" type="submit" value="Üyeliği Tamamla" />
                    </div><!-- /. sagBolum -->
                </div><!-- /. digerBilgiler -->
            </div><!-- /. resimYuklemeAlani -->
        </div><!-- /. uyeOlAdim2 -->

        <div id="avatarSecPop" class="simplePop">
            <div class="baslikBolumu">
                <span class="baslik">Avatar Seç</span>
                <a class="avatarPopKapat" href="#"><i class="fa fa-times"></i></a>
            </div><!-- /. baslikBolumu -->
            <div class="avatarlar">
                <div class="avatarlarTutucu">
                    <ul>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar1.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar2.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar3.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar4.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar5.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar6.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar7.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar8.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar9.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar10.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar11.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar12.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar13.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar14.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar15.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar16.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar17.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar18.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar19.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar1.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar2.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar3.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar4.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar5.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar6.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar7.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar8.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar9.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar10.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar11.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar12.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar13.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar14.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar15.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar16.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar17.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar18.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar19.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar1.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar2.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar3.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar4.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar5.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar6.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar7.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar8.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar9.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar10.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar11.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar12.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar13.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar14.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar15.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar16.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar17.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar18.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar19.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar1.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar2.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar3.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar4.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar5.jpg" alt="avatar"/></a></li>
                        <li><a href="#"><img src="assets/thumb/avatarlar/avatar6.jpg" alt="avatar"/></a></li>
                    </ul>
                </div><!-- /. avatarlarTutucu -->
            </div><!-- /. avatarlar -->
        </div><!-- /. avatarSecPop -->

        <div id='fotograflarDetay' class="simplePop">
            <div class="buyukFotograf">
                <a class="ok oncekiSlider" href="#"><i class="fa fa-angle-left"></i></a>
                <img src="" alt="..." />
                <a class="ok sonrakiSlider" href="#"><i class="fa fa-angle-right"></i></a>
                <a class="fotografDetayPopKapat" href="#"><i class="fa fa-times"></i></a>
                <span class="resimNumarasi">7/25</span>
            </div>
            <div class="fotoDetaySlider"></div><!-- /. fotoDetaySlider -->
        </div><!-- /. fotograflarDetay -->

        <div id='haritaPop' class="simplePop">
            <a class="haritaPopKapat" onclick="haritaPop.close();" href="#"><i class="fa fa-times"></i></a>
            <div id="vmap" style="width: 1024px; height: 768px;"></div>
        </div><!-- /. haritaPop -->        <script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="assets/js/jquery.lazyload.js"></script>
        <script type="text/javascript" src="assets/js/abkit.js"></script>
        <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.elevatezoom.js"></script>
        <script type="text/javascript" src="assets/js/select2.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.qtip.min.js"></script>
        <script type="text/javascript" src="assets/js/dropzone.js"></script>
        <script type="text/javascript" src="assets/js/jquery.vmap.js"></script>
        <script type="text/javascript" src="assets/js/jquery.vmap.turkey.js"></script>
        <script type="text/javascript" src="assets/js/jquery.easing.min.js"></script>
        <script type="text/javascript" src="assets/js/masterslider.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.nanoscroller.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="assets/js/verticalScrolling.js"></script>
        <script type="text/javascript" src="assets/js/ehaber.js"></script>
    </body>
</html>