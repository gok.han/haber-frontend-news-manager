/*eslint no-console */
// const feedURL = 'http://skati.herokuapp.com/son_dk'
const feedURL = 'http://localhost:2222/son_dk'

// METHODS
// -----------
// Custom AJAX Promise
function get(url) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    console.log('GET', url)
    req.open('GET', url);
    req.onload = () => req.status === 200 ? resolve(req.response) : reject(Error(req.statusText));
    console.log('LOADED', url)
    req.onerror = (e) => reject(Error(`Network Error: ${e}`));
    req.send();
  });
}


// MARKUPS
// takes an employee and turns it into a block of markup
function haber_markup (item) {
  // put ._fcm-result-view div added to the items  you want to be visible in the list
  return `<li>
     <div class="_fcm-result-view">
          <img class="_fcm-result-view__img" src="${item.image_url}"/>
          <div class="_fcm-result-view__title">${item.title}</div>
      </div>
      <a href="anaSayfa.php.html#" class="haberGorsel">
          <img src="${item.image_url}"/>
      </a>
      <div class="haberDetay">
          <a href="anaSayfa.php.html#"><strong>${item.title}</strong></a>
          <p>${item.image_alt_info}</p>
          <time class="tarih">${item.date}</time>
          <span class="yorum"><i class="fa fa-comment-o"></i>${item.stat.count_comment}</span>
      </div><!-- /. haberDetay -->
  </li>`
}

function search_item_markup (item) {
  return `<div class="autocomplete-suggestion" data-slug="${item.slug}"> ${item.title}</div>`
}

// stuff to do after the page loaded

document.addEventListener("DOMContentLoaded",function(){
  console.log('DOMContentLoaded FEMAN ready');
  const habeler_liste = document.querySelector('.haberListelemeUl')
  const sonuclar_liste = document.querySelector('._fcm-results')

  let search_input = document.querySelector('input[name="q"]');


function add_from_search (slug) {
  // Request Haber List
  get(feedURL + '?slug=' + slug)
  // limit initial results only by 10
    .then((data) => {
      let selected = JSON.parse(data)
      sonuclar_liste.insertAdjacentHTML("afterbegin", haber_markup(selected[0]))
    })
    .catch((err) => {
      console.log('There is an error oldu yaf search item eklerkene', err)
    });
}

    // Request Haber List
    get(feedURL + '?_start=0&_end=10') // limit initial results only by 10
      .then((data) => {
        // console.log(data)
        let haberler = JSON.parse(data)

        haberler.forEach((item, i) => {
            habeler_liste.innerHTML += haber_markup(item)
          });
      })
      .catch((err) => {
        console.log('There is haber çekerken error oldu yaf', err)
      });

    // https://goodies.pixabay.com/javascript/auto-complete/demo.html
    new autoComplete({
      selector: 'input[name="q"]',
      source: function(term, response){
          let suggestions = [];
          get(feedURL + `?_start=0&_end=10&q=${term}`)
          // limit initial results only by 10
            .then((data) => {
              let results = JSON.parse(data)
              for (let i=0; i<results.length; i++) {
                suggestions.push({
                  title: results[i].title,
                  slug: results[i].slug
                });
              }
            }).then(() => {
              response(suggestions);
            })
            .catch((err) => {
              console.log('There is an autocomplete error oldu yaf', err)
            });
      },
      renderItem: function (item, search){
        // template desing the markup section
        return search_item_markup(item);
      },
      onSelect: function(event, term, item) {
        search_input.value = ''
        add_from_search(item.dataset.slug)
      }
  });
});
let editFlag = false;
const fcm_editor = '';
let fcb_currently_edited = ''
function editorActivate (managedItem, editor) {
      editor = (!!editor) ? (editor) : (document.querySelector('._fcm-editor'));
      const lastManaged = (managedItem) ? managedItem : 'null' // hack for empty resize
      managedItem = (!!managedItem) ? managedItem : lastManaged // hack for empty resize

      // const managedItemPos = managedItem.getBoundingClientRect();
      
      editor.style.left = eval(managedItem.offsetWidth + managedItem.offsetLeft) + 'px';

        console.log('IFELSEEE', window.scrollY, managedItem.offsetTop)
      if (window.scrollY > managedItem.offsetTop) {
        console.log('IF')
        editor.style.top = '0';
        apollo.addClass(editor, '-is-fixed');
      } else {
        console.log('ELSEEE')
        editor.style.top = managedItem.offsetTop + 'px';
        // editor.style.top = '920px'; // SHAME
        apollo.removeClass(editor, '-is-fixed');
      }
  }

window.addEventListener( "load", function(){
  console.log('hi bitch I am totally loaded')

  let currentlyEditingEl = document.querySelector('.-is-editable');
  const fcm_editor = document.querySelector('._fcm-editor');

  

    // let feed_sortable = document.querySelector('._fcm-block');
    let feed_searchable = document.querySelector('._fcm-results');
    let managables = document.querySelectorAll('.-is-editable');

    

   managables.forEach((manageble, i) => {

     console.log('editFlag', editFlag)

     manageble.addEventListener('click', (e) => {
     fcb_currently_edited = manageble;
       /// Sorting action options
       const sortOptionsFeed = {
         group: {
           name: 'feed',
           put: 'result',
         },

         onMove: function (evt) {
           let itemEl = evt.item; // dragged HTMLElement
           let order = thesortable.toArray();
           console.log('sorted', order)
         },
         animation: 200
       }
       const sortOptionsSearch = {
         group: {
           name: "result",
           put: 'feed',
         },
         onMove: function (evt) {
           let itemEl = evt.item; // dragged HTMLElement
           let order = thesearchable.toArray();
           console.log('sorted', order)
         },
         animation: 200
       }

       if (!!editFlag === false) {
          console.log('editFlag', e)
          
          apollo.addClass(e.target, '-is-currently-being-edited');
          var thesortable = new Sortable(e.target.querySelector('ul'), sortOptionsFeed);
          var thesearchable = new Sortable(feed_searchable, sortOptionsSearch);
          editFlag = true

       } else {
         console.log('still liste')
       }

       // activate and repos the editor
       if(editFlag) {
        editorActivate(manageble);
      }
      //  console.log('managable clicked', e)

     })

    
   });

}, false );
document.addEventListener('scroll', () => {
  console.log('scrolling')
  editorActivate(fcb_currently_edited);
})

// window.addEventListener('resize', this.moveInnerRelateds)
window.addEventListener('resize', function() {
    if(editFlag) {
      editorActivate(fcb_currently_edited);
    }
}, true);