/*
*   Dokuman  : ehaber.com
*   Tarih    : 4.8.2015
*   Yazar    : Arayuz ~ www.arayuz.com.tr
*   Tarayici : IE9,IE10,IE11, FIREFOX, CHORME, SAFARI
*/

/**
 * Anasayfa Fonksiyonlar
 **/
    anaSayfaHavaDurumuListe();
    menu();
    anaSayfaTeknolojiSlider();
    anaSayfaSporSlider();
    anaSayfaGundemdekiKisiler();
    cokOkunanTab();
    uyeGirisi();
    takvimler();
    lazy();
    masterSlider();
/**
 * Detay Sayfasi Fonksiyonlar
 **/
    haberDetaySagTab();
    sagTab();
    haberOzellestir();
    detayYorumlar();
    yorumCevapla();
    yorumMiktari();
    tumUlkelerListe();
    yorumBildir();
    resimYuklePop();
    yorumGonder();
    videoEklePop();
    tooltipler();
/**
 * Finans Sayfasi Fonksiyonlar
 **/
    istatistikBar();
    hisseler();
/**
 * Spor Sayfa Fonksiyonlar
 **/
    fiksturTab();
    takimSlider();
/**
 * Gazeteler Sayfa Fonksiyonlar
 **/
    gazetelerSlider();
/**
 * Gazete Sayfa Fonksiyonlar
 **/
    gazeteZoom();
/**
 * Webtv Video Detay Sayfa Fonksiyonlar
 **/
    paylasSiteneEkleTab();
    siteneEkle();
/**
 * Biyografi Sayfa Fonksiyonlar
 **/
    alfabe();
    kategori();
    populerite();
    fotografDetayPop();
    detayFotoSlider();
/**
* 404 Sayfasi
**/
    say404( 'http://www.ehaber.com/' );
/**
* Profil Sayfasi
**/
    profilTab();
    uyeBildir();
    uyeBildirildi();
    headerProfile();
    kullaniciMenusu();
    okunmamisBildirimler();
    uyeBildirMenu();
    siteneEkleKopyalama();
    yorumaCevapYaz();
    ilkYorumGonderme();
    yorumBildir2();
    resimYukleme();
    resimYukleme2();
    resimYukleme3();
    takipEt();
/**
* Uye Girisi Popuplar
**/
    uyeGirisiPop();
    uyelikFormKontrolu();
    avatarSecme();
    /**
* mesaj Popup
**/
    mesajPop();
/**
* Sehir Haberler Sayfasi
**/
    harita();
/**
* Kurumsal Sayfalari
**/
    cvFile();
/**
* Arama Sayfalari
**/
    nano();
    aramaBolumu();
/**
* Galeri Sayfalari
**/
    galeriYon();
/**
* Kategori Sonuc Sayfalari
**/
    kategoriSonucSlider();
/**
* IHA Tumu
**/
    acilirMenu2();
    
/*Selectler*/
$( '.select2' ).select2({
    dropdownCssClass: "aycan",
    formatNoMatches: function( term ) {
        var flag = 1;
        $( '.select2-input' ).on( 'keyup', function(e) {
            if(e.keyCode === 13) {
                $( "#modalAdd" ).modal();
                $( ".select2-input" ).unbind( "keyup" );
            }
        });
        return "<li class='select2-no-results'>"+"Sonuç bulunamadı..</li>";
    }
});

$(".fancybox").fancybox({
        openEffect	: 'none',
        closeEffect	: 'none'
});
/*Yorum Bolumu Select*/
$( '.yorumlarSelect2' ).select2({
    dropdownCssClass: "yorumlarSelect",
    formatNoMatches: function( term ) {
        var flag = 1;
        $( '.select2-input' ).on( 'keyup', function(e) {
            if(e.keyCode === 13) {
                $( "#modalAdd" ).modal();
                $( ".select2-input" ).unbind( "keyup" );
            }
        });
        return "<li class='select2-no-results'>"+"Sonuç bulunamadı..</li>";
    }
});

$( '.select2-arrow' ).append( '<i class="fa fa-angle-down"></i>' );

/*Lazyload*/
function lazy() { 
    $( "img.lazy" ).lazyload({
           effect : "fadeIn"
    });
}

/*Checkbox*/
checks = $( ".abCheckbox" ).abCheckbox();

checks = $( ".abCheckbox2" ).abCheckbox({
    onChange : function (e) {
        e.closest( 'label' ).toggleClass( 'secili' );
    },
});

/*Radio*/
radios = $( ".abRadio" ).abRadio();


function anaSayfaHavaDurumuListe()
{
    hedefDisiTiklama(['.tumSehirlerListe','.havaDurumu'],function(){
        $( '.tumSehirlerListe' ).fadeOut(250);
        $( '.tumSehirler' ).removeClass( 'acik' );
    });
    $( '.havaDurumu' ).on( 'click',function (e){
        if($( this ).hasClass( 'acik' ))
        {
            $( this ).removeClass( 'acik' );
            $( '.tumSehirlerListe' ).fadeOut(250);
        } else {
            $( this ).addClass( 'acik' );
            $( '.tumSehirlerListe' ).fadeIn(250);
        }
    });
}

function menu(kapat){
    $( 'li.tumu a.tumu' ).on( 'click',function (){
        hedefDisiTiklama(['.tumMenuler','.tumu'],function(){
            $( 'li.tumu' ).removeClass( 'acik' );
            $( '.tumMenuler' ).fadeOut(250);   
        }); 
       if($( this ).parent().hasClass( 'acik' ))
       {
           $( this ).parent().removeClass( 'acik' );
           $( this ).parent().find( '.tumMenuler' ).fadeOut(250);
       }else {
           $( this ).parent().addClass( 'acik' );
           $( this ).parent().find( '.tumMenuler' ).slideDown(250);
       }
    });
}

function kategoriSonucSlider(){
    
    var owl = $( ".kategoriSonucSlider .ajansHaberler" );
    
    owl.owlCarousel({
        loop:true,
        lazyLoad : true,
        navigation : false, 
        slideSpeed : 300,
        pagination: false,
        items : 5
    });
    
    $( ".kategoriSonucSlider .ajansHaberler" ).each(function (){
        if($(this).find('li').size() > 5)
        {
            $(this).parent().append('<a href="#" class="kategoriSonucButon kategoriSonucIleri">prev</a>');
            $(this).parent().prepend('<a href="#" class="kategoriSonucButon kategoriSonucGeri">prev</a>');
            $( '.kategoriSonucIleri' ).on( 'click',function (e){
                e.preventDefault(); 
                $(this).closest('.kategoriSonucSlider').find('.ajansHaberler').trigger( 'owl.next' );
            });
            $( '.kategoriSonucGeri' ).on( 'click',function (e){
                e.preventDefault(); 
                $(this).closest('.kategoriSonucSlider').find('.ajansHaberler').trigger( 'owl.prev' );
            });
            
        }
    })
    

}

function anaSayfaTeknolojiSlider(){
    
    var owl = $( "#teknolojiSlider" );
    owl.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        thumbs: true,
        thumbImage: true,
        lazyLoad : true
    });
    
    $( '.teknolojiBolumu .ileriGeri a.ileri' ).on( 'click',function (e){
        e.preventDefault(); 
        owl.trigger( 'owl.next' );
    });

    $( '.teknolojiBolumu .ileriGeri a.geri' ).on( 'click',function (e){
        e.preventDefault(); 
        owl.trigger( 'owl.prev' );
    });

    $('#teknolojiSlider .owl-page').mouseover(function () {
        var sira = $(this).index();
        owl.trigger('owl.goTo', sira);
    });

}


function anaSayfaGundemdekiKisiler(){
    
    var owl3 = $( "#gundemdekiKisilerSlider" );
    owl3.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        singleItem:true,
        pagination:false
    });
    
    $( '.gundemdekiKisiler  .ileriGeri a.ileri' ).on( 'click',function (e){
        e.preventDefault(); 
        owl3.trigger( 'owl.next' );
    });
    $( '.gundemdekiKisiler .ileriGeri a.geri' ).on( 'click',function (e){
        e.preventDefault(); 
        owl3.trigger( 'owl.prev' );
    });
}

function anaSayfaSporSlider(){
    
    var owl2 = $( "#sporSlider" );
    owl2.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        singleItem:true,
        pagination: false
         
    });
    
    $( '.sporHaberleri  .ileriGeri a.ileri' ).on( 'click',function (e){
        e.preventDefault(); 
        owl2.trigger( 'owl.next' );
    });
    $( '.sporHaberleri  .ileriGeri a.geri' ).on( 'click',function (e){
        e.preventDefault(); 
        owl2.trigger( 'owl.prev' );
    });
}


function cokOkunanTab(){
    var tab = $( '.cokOkunanlar' ).simpleTab({
        tabEffect   : 'fadeIn',
        specialTab  : true,
        tabLink     : ".cokOkunanlar > .tabUstBorder >  .tabLink",
        tabLinkA    : ".cokOkunanlar > .tabUstBorder > .tabLink > a",
        tabContent  : ".cokOkunanlar > .tabContent",
    });
}

/*Haber Detay Slider*/
//$( ".haberAltSlider ul" ).owlCarousel({
//    items : 1,
//    pagination: false,
//    navigation : true,
//    
//});

/*okuma Modu*/
$( ".okumaModu" ).click(function() {

    if ($( '.okumaModu' ).hasClass( 'secili' )) {
        okumaModuKapat();
    } else {
        $( this ).addClass( 'secili' );
        $( 'body' ).append($( '<div/>' ).addClass( 'kararti' ).css({
            background  : "rgba(0,0,0,0.7)",
            position    : "absolute",
            left        : 0,
            top         : 0,
            width       : '100%',
            height      : $(document).outerHeight(),
            zIndex      : 90
        }));
        $( '.icerikSol' ).addClass( 'okumaModu' );

    }

    $(document).on( "keydown",function(e){
        if ($( '.okumaModu' ).hasClass( 'secili' )) {

            if(e.keyCode == 27){
                okumaModuKapat();
            }
        }
    });
    hedefDisiTiklama(['.icerikSol'],okumaModuKapat);
});

function okumaModuKapat(){
    $( '.kararti' ).fadeOut(250,function (){
        $( this ).remove();
    });
    $( '.okumaModu' ).removeClass( 'secili' );
    $( '.icerikSol' ).removeClass( 'okumaModu' );
}

function haberDetaySagTab(){
    var tab = $( '.haberDetaySagTab' ).simpleTab({
        tabEffect   : 'fadeIn',
        event       : "mouseover",
        speed       : 0,
        auto        : true,
        autoSpeed   : 2000
    });
}
function sagTab(){
    var tab = $( '.sagTab' ).simpleTab({
        tabEffect   : 'fadeIn',
        event       : "mouseover",
        speed       : 0,
        auto        : true,
        autoSpeed   : 2000,
        nextArrow   : $('.sagHaberNext'),
        prevArrow   : $('.sagHaberPrev'),
        uniqInterval: "sagTab"
    });
}

function hisseler(){
    var tab = $( '.hisselerTab' ).simpleTab({
        tabEffect : 'fadeIn',
        speed       : 0,
    });
}

function profilTab(){
    var tab = $( '.profilTab' ).simpleTab({
        tabEffect : 'fadeIn',
        speed       : 0,
    });
}

function gazetelerSlider(){
    var owl = $( ".gazeteSlider" );
    owl.owlCarousel({
        navigation : true, 
        slideSpeed : 300,
        pagination: false,
        items : 9,
        lazyLoad : true,
        navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    });
}

function takimSlider(){
    var owl = $( ".takimSlider ul" );
    owl.owlCarousel({
        navigation : true, 
        slideSpeed : 300,
        pagination: false,
        items : 11,
        lazyLoad : true,
        navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    });
}

function haberOzellestir(){
    $( '.haberOzellestir a.buyuk' ).click(function (){
        $( '.haberIlkAciklama .haberBaslik, .haberIlkAciklama .haberAltBaslik, .haberIcerik *, .haberIcerik2 *' ).finish();
        $( '.haberIlkAciklama .haberBaslik, .haberIlkAciklama .haberAltBaslik, .haberIcerik *, .haberIcerik2 *' ).animate({
           fontSize : "+=2"
        },250);
    });
    $( '.haberOzellestir a.kucuk' ).click(function (){
        $( '.haberIlkAciklama .haberBaslik, .haberIlkAciklama .haberAltBaslik, .haberIcerik *, .haberIcerik2 *' ).finish();
        $( '.haberIlkAciklama .haberBaslik, .haberIlkAciklama .haberAltBaslik, .haberIcerik *, .haberIcerik2 *' ).animate({
           fontSize : "+=-2"
        },250);
    });
}

function pixel(){
    setInterval(function(){
        $('.pixelPerfect').css('zIndex',9999);
        $('.pixelPerfect').fadeIn(500,function(){
            $(this).fadeOut(500);
        });
    },1000)
}
$('.pixelPerfect').remove();
function takvimler() { 
    $( ".takvimAc2" ).datepicker();
    $( ".takvimAc" ).datepicker();

    $( ".takvimAc" ).focus(function(event) {
        $( this ).addClass( 'secili' );
    });
    $( ".takvimAc" ).focusout(function(event) {
        $( this ).removeClass( 'secili' );
    });

    $('.takvimAc').on("changeDate", function() {
        var tarih = $(this).datepicker('getFormattedDate');
        var secili = $('.day.active').index();
        var gun = '';

        if (secili == 0) {
            gun = ' Pazartesi';
        } else if (secili == 1) {
            gun = ' Salı';
        } else if (secili == 2) {
            gun = ' Çarşamba';
        } else if (secili == 3) {
            gun = ' Perşembe';
        } else if (secili == 4) {
            gun = ' Cuma';
        } else if (secili == 5) {
            gun = ' Cumartesi';
        } else if (secili == 6) {
            gun = ' Pazar';
        } else {};

        $(this).html('<i class="fa fa-bars"></i>'+tarih+gun);
    });

}

$( '.canliSkorMenu .ses' ).click(function(event) {
    event.preventDefault();
    $( this ).toggleClass( 'secili' );
});

function detayYorumlar(){
    $( '.yorumButon .begen' ).click(function(e){
        e.preventDefault()
        $( this ).html( '<i class="fa fa-thumbs-up"></i>'+($( this ).text()*1 + 1));
    });
    $( '.yorumButon .begenme' ).click(function(e){
        e.preventDefault()
        $( this ).html( '<i class="fa fa-thumbs-down"></i>'+($( this ).text()*1 + 1));
    });
    
    
    $('.begen a.begenA').click(function(e){
        e.preventDefault()
        $( this ).html( '<i class="fa fa-thumbs-up"></i>'+($( this ).text()*1 + 1));
    });
    
    $('.begen a.begenmeA').click(function(e){
        e.preventDefault()
        $( this ).html( '<i class="fa fa-thumbs-down"></i>'+($( this ).text()*1 + 1));
    });
    
}

function istatistikBar(){
    $( '.barTutucu .bar span' ).each(function (){
        $( this ).css( 'width',$( this ).attr( 'data-yuzde' ));
    });
}

function yorumCevapla(){
    $( '.yorumlar .cevapla' ).click(function(event) {
        $( '.yorumlar' ).find( '.yorumYaz' ).not( '.ilkYorum' ).slideUp();
        $( '.yorumlar' ).find( '.yorumuBildir' ).slideUp();
        event.preventDefault();
        $( this ).closest( 'li' ).find( '.yorumYaz' ).finish().slideDown();
    });
}

function yorumBildir(){
    $( '.yorumButon .bildir' ).click(function(event) {
        $( '.yorumlar' ).find( '.yorumYaz' ).not( '.ilkYorum' ).slideUp();
        $( '.yorumlar' ).find( '.yorumuBildir' ).slideUp();
        event.preventDefault();
        $( this ).closest( 'li' ).find( '.yorumuBildir' ).finish().slideDown();
    });
}

function yorumMiktari(){
    $( '.yorumYaz textarea' ).keyup(function(event) {
        var sayi = $( this ).val().length;
        $( this ).closest( 'li' ).find( '.yorumSinir' ).html(1000-sayi);
    });

    $( '.yorumYazAlt textarea' ).keyup(function(event) {
        var sayi = $( this ).val().length;
        $( this ).closest( '.yorumYazAlt' ).find( '.yorumSinir' ).html(1000-sayi);
    });
}



function gazeteZoom(){
    $( ".gazeteZoom" ).elevateZoom({
        constrainType:"height", 
        constrainSize:200, 
        zoomType: "lens", 
        containLensZoom: true,
    }); 
}


function fiksturTab(){
    var tab = $( '.fikstur' ).simpleTab();
}


function tumUlkelerListe(){
    $( '.ulkeleriAc' ).on( 'click',function (e){
        if($( this ).hasClass( 'acik' ))
        {
            $( this ).removeClass( 'acik' );
            $( '.tumUlkelerListe' ).fadeOut(250);
        } else {
            $( this ).addClass( 'acik' );
            $( '.tumUlkelerListe' ).fadeIn(250);
        }
    });
    hedefDisiTiklama(['.tumUlkelerListe','.ulkeleriAc'],function(){
        $( '.tumUlkelerListe' ).fadeOut(250);
        $( '.ulkeleriAc' ).removeClass( 'acik' );
    });
}

// $( '.yorumYaz input[type="submit"]' ).click(function(event) {
//     event.preventDefault();

//     var yorum = $( this ).closest( '.yorumYaz' ).find( 'textarea' ).val();
//     var isim = $( this ).closest( '.yorumYaz' ).find( 'input[type="text"]' ).val();
//     var DOM = '';
//     //Saat icin
//     var d = new Date();
//     var saat = d.getHours(); 
//     var dakika = d.getMinutes(); 

//     DOM += '<li class="yeniYorum">'
//     DOM +=     '<i class="sprite"></i>'
//     DOM +=     '<span class="isim">'+(isim)+'</span>'
//     DOM +=     '<p>'+(yorum)+'</p>'
//     DOM +=     '<div class="yorumButon">'
//     DOM +=         '<span class="yorumOnay">Yorumunuz onay bekliyor.</span>'
//     // DOM +=         '<time class="tarih">'+(saat)+':'+(dakika)+'</time>'
//     DOM +=         '<time class="tarih">Birkaç dakika önce..</time>'
//     DOM +=     '</div>'
//     DOM += '</li>'

//     $( this ).closest( 'ul' ).find( '> .yorumYaz' ).after(DOM);
// });

function paylasSiteneEkleTab(){
    var tab = $( '.paylasSiteneEkleTab' ).simpleTab({
        tabEffect   : 'fadeIn',
    });
}


function siteneEkle(){
    $( '.siteneEkleSol input' ).on( 'keyup', function(event) {
        $( this ).val($( this ).val().replace(/\D+/g,'' ))
        var genislik = $( '.siteneEkleSol .genislik' ).val();
        var yukseklik = $( '.siteneEkleSol .yukseklik' ).val();
        var DOM  = '<iframe width="'+genislik.replace(/\D+/g,'' )+'" height="'+yukseklik.replace(/\D+/g,'' )+'" src="https://www.youtube.com/embed/IN7NmvlAKOM" frameborder="0" allowfullscreen></iframe>';
        $( '.siteneEkleSag textarea' ).val(DOM);

        /*Sitene Ekle Kopyala Butonu*/
        $( '.siteneEkleSag .kopyala' ).html( 'Kopyala' );
    });
}


function alfabe(kapat){
    $( '.alfabeAc' ).on( 'click',function (){
        hedefDisiTiklama(['.alfabeTutucu'],function(){
            $( '.alfabeAc' ).removeClass( 'aktif' );
            $( '.alfabe' ).fadeOut(250);
        }); 
       if($( this ).hasClass( 'aktif' ))
       {
           $( this ).removeClass( 'aktif' );
           $( this ).parent().find( '.alfabe' ).fadeOut(250);
       }else {
           $( this ).addClass( 'aktif' );
           $( this ).parent().find( '.alfabe' ).slideDown(250);
       }
    });
}



function kategori(kapat){
    $( '.kategoriAc' ).on( 'click',function (){
        hedefDisiTiklama(['.kategorilerTutucu'],function(){
            $( '.kategoriAc' ).removeClass( 'aktif' );
            $( '.kategori' ).fadeOut(250);
        }); 
       if($( this ).hasClass( 'aktif' ))
       {
           $( this ).removeClass( 'aktif' );
           $( this ).parent().find( '.kategori' ).fadeOut(250);
       }else {
           $( this ).addClass( 'aktif' );
           $( this ).parent().find( '.kategori' ).slideDown(250);
       }
    });
}



function populerite(){
    $( '.populerite' ).click(function(event) {
        $( this ).toggleClass( 'aktif' );
    });
}


function tooltipler() { 
    $( '.tooltip' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.tooltiptext' )
            },
            position: {
                my: 'bottom right',
                at: 'top right',
                adjust: {
                    x: 28,
                    y: -0
                }
            }
        });
    });
    $( '.ajansTooltip' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.tooltip' )
            },
            position: {
                my: 'bottom right',
                at: 'top right',
                adjust: {
                    x: 10,
                    y: -0
                }
            }
        });
    });
    $( '.ajansTooltipWrapper' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.ajansTooltip' )
            },
            position: {
                my: 'bottom left',
                at: 'top left',
                adjust: {
                    x: -85,
                    y: 0
                }
            }
        });
    });
    $( '.ajansTooltipWrapper.ajans1' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.ajansTooltip' )
            },
            position: {
                my: 'bottom left',
                at: 'top left',
                adjust: {
                    x: -75,
                    y: 0
                }
            }
        });
    });
    $( '.ajansTooltipWrapper.ajans2' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.ajansTooltip' )
            },
            position: {
                my: 'bottom left',
                at: 'top left',
                adjust: {
                    x: -72,
                    y: 0
                }
            }
        });
    });
    $( '.siteneEkleTooltip' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.siteEkleTooltipText' )
            },
            position: {
                at: 'top center',
                my: 'bottom center',
                adjust: {
                    x: -60,
                    y: +5
                }
            }
        });
    });

    $( '.tooltip4' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.tooltiptext4' )
            },
            position: {
                my: 'bottom left',
                at: 'top left',
                adjust: {
                    x: -25,
                    y: -0
                }
            }
        });
    });

    $( '.tooltip5' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.tooltiptext5' )
            },
            position: {
                my: 'bottom left',
                at: 'top left',
                adjust: {
                    x: -18,
                    y: 16
                }
            }
        });
    });

    $( '.tooltip2' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).closest( 'li' ).find( '.tooltiptext2' )
            },
            position: {
                my: 'top left',
                at: 'bottom left',
                adjust: {
                    x: -28,
                    y: 0
                }
            }
        });
    });


    $( '.tooltip3' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.tooltiptext3' )
            },
            position: {
                my: 'top left',
                at: 'bottom left',
                adjust: {
                    x: 10,
                    y: 3
                }
            }
        });
    });
    $( '.tooltipMesaj' ).each(function() {
        $( this ).qtip({
            content: {
                text: $( this ).find( '.mesajtooltip' )
            },
            position: {
                my: 'bottom left',
                at: 'top left',
                adjust: {
                    x: -20,
                    y: 0
                }
            }
        });
    });

}

function detayFotoSlider(){
    owlBiyografi = $( ".fotoDetaySlider ul" );
    owlBiyografi.owlCarousel({
        navigation : true, 
        slideSpeed : 300,
        pagination: false,
        items : 5,
        lazyLoad : true,
        navigationText : ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
    });
}


function say404(site){
    setInterval(function(){

        var saniye = parseInt($( '.yonlendirme strong' ).html(),10) - 1;

        if(saniye  < 0){   
            $( '.yonlendirme' ).html( '<strong>Yönlendiriliyorsunuz...</strong>' );
            window.location.href = site;
        } else {
            $( '.yonlendirme strong' ).html(saniye);
        }

    },1000)
}

videoHataPop =  $( '#videoEklePop2' ).simplePop();

haritaPop =  $( '#haritaPop' ).simplePop();

uyeOlAdim2Pop =  $( '#uyeOlAdim2' ).simplePop();

avatarPop =  $( '#avatarSecPop' ).simplePop();
$( '.avatarPopKapat' ).click(function(e){
    e.preventDefault();
    avatarPop.close();
});

function resimYuklePop(){
    pop1 =  $( '#resimYukle' ).simplePop();
    $( '.popKapat1' ).click(function(e){
        e.preventDefault();
        pop1.close();
    });
}

function yorumGonder(){
    pop2 =  $( '#yorumGonderildi' ).simplePop();
    $( '.popKapat2' ).click(function(e){
        e.preventDefault();
        pop2.close();
    });
}

function uyeBildir(){
    pop5 =  $( '#uyeBildir' ).simplePop();
    $( '.popKapat5' ).click(function(e){
        e.preventDefault();
        pop5.close();
    });
}

function videoEklePop(){
    pop3 =  $( '#videoEklePop' ).simplePop();
    $( '.popKapat3' ).click(function(e){
        e.preventDefault();
        pop3.close();
    });

    //Video dogrulugunun kontrolu
    $( '#videoGonder' ).click(function(event) {
        event.preventDefault();
        var videoLink = $( '#videoLink' ).val();

        if( ( videoLink.indexOf('youtube.com/') > -1 ) ){
            // Youtube icin
                var embedLink = jQuery.trim(videoLink.split('watch?v=')[1]);
                var DOM = '';
                
                // iframe olarak yukleme
                // DOM += '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+embedLink+'" frameborder="0" allowfullscreen></iframe>';
                
                // resim olarak yukleme
                DOM += '<img src="http://img.youtube.com/vi/'+embedLink+'/1.jpg" >';
                
                $('#eklenenDosya').empty();
                $('.ilkYorum textarea').addClass('dosyaEklendi');
                $('#eklenenDosya').append(DOM).append('<a class="ekliDosyayiKaldir" href="#"><i class="fa fa-times-circle"></i></a>');
                $('.ekliDosyayiKaldir').click(function(event) {
                    event.preventDefault();
                    $('#eklenenDosya').empty();
                    $('.ilkYorum textarea').removeClass('dosyaEklendi');
                });
                pop3.close();
          
        } else if(( videoLink.indexOf('izlesene.com/') > -1 )) {

            alert("İzlesene Parse");

        } else {

            pop3.close(function() {
                videoHataPop.open();
            });

        }
    });

    $('.videoEkle .tekrarDene').click(function(event) {
        event.preventDefault();
        videoHataPop.close(function() {
            pop3.open();
        });
    });
}


function fotografDetayPop(){
    fotograflarPop = $('#fotograflarDetay').simplePop({
        open : function(){
            var resim,
                index;
            //ilk buyuk resmi olusturma
            if( $('.detaySlider .owl-item.aktif').index() < 0 ){
                // ilk resim
                resim = $( '.fotoDetaySlider .owl-item:first-child img' ).attr( 'data-orginal-img' );
            } else {
                // tiklanan resim
                index = $('.detaySlider .owl-item.aktif').index();
                resim = $('.detaySlider .owl-item.aktif img').attr( 'data-orginal-img' );
                $( '.fotoDetaySlider .owl-item' ).eq(index).addClass('aktif');
            }

            $( '.buyukFotograf img' ).attr( 'src', resim);
            fotograflarPopSayiSira();
        },
        close : function(){
            $( '.buyukFotograf img' ).attr( 'src', '');
        }
    });

    $( '.fotografDetayPopKapat' ).click(function(event){
        event.preventDefault();
        fotograflarPop.close();
    });

    //slider olusturma
    var html = $( '.detaySlider .sliderTutucu2' ).html();
    $( '.fotoDetaySlider' ).append(html);

    // sliderin sayisini belirleyip yazdirma
    fotograflarPopSayiSira();

    // slideri calistirma
    var owl = $( ".detaySlider ul" );
    owl.owlCarousel({
        navigation : true, 
        slideSpeed : 300,
        pagination: false,
        items : 4,
        lazyLoad : true,
        navigationText : ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
        afterInit : function(e){
            e.find('a').on('click',function(){
                var aktifResim = $(this).find('img').attr( 'data-orginal-img' );
            })
        }
    });

    // li lere tiklandikca resmi degistirme
    $( '.fotoDetaySlider ul li' ).click(function(event) {
        event.preventDefault();
        $('.fotoDetaySlider .owl-item').removeClass('aktif');
        $(this).closest('.owl-item').addClass('aktif');

        var src  = $( this ).find( 'img' ).attr( 'data-orginal-img' );
        $( '.buyukFotograf img' ).attr( 'src', src);

        //Buraya slider sayısını hesaplattırma yapılacak !!!
    });

    // onceki ve sonraki butonu slider resmi degistirme
    $( '#fotograflarDetay .oncekiSlider' ).click(function(event) {
        event.preventDefault();
        sira = $( '.detaySlider .owl-item.aktif' ).index();
        if (sira < 0) {

        } else {
            $('.detaySlider .owl-item').removeClass('aktif');
            $('.detaySlider .owl-item').eq(sira-1).addClass('aktif');
            var src = $( '.detaySlider .owl-item' ).eq(sira-1).find( 'img' ).attr( 'data-orginal-img' );
            $( '.buyukFotograf img' ).attr( 'src', src);
            fotograflarPopSayiSira();
        };
        
    });

    // onceki ve sonraki butonu slider resmi degistirme
    $( '#fotograflarDetay .sonrakiSlider' ).click(function(event) {
        event.preventDefault();
        sira = $( '.detaySlider .owl-item.aktif' ).index();
        if (sira == $( '.detaySlider .owl-item').size()-1) {

        } else {
            $('.detaySlider .owl-item').removeClass('aktif');
            $('.detaySlider .owl-item').eq(sira+1).addClass('aktif');
            var src = $( '.detaySlider .owl-item' ).eq(sira+1).find( 'img' ).attr( 'data-orginal-img' );
            $( '.buyukFotograf img' ).attr( 'src', src);
            fotograflarPopSayiSira();
        };

    });
    
}

/*detay sliderdan popup acma*/
$('.detaySlider ul li a').click(function(e) {
    e.preventDefault();
    $('.detaySlider .owl-item').removeClass('aktif');
    $(this).closest('.owl-item').addClass('aktif');
    fotograflarPop.open();
});

function fotograflarPopSayiSira(){
    var resimSayisi  = $( '.detaySlider ul li' ).size();
    var aktifResim   = $('.detaySlider .owl-item.aktif').index()+1;
    $( '#fotograflarDetay .resimNumarasi' ).html( ' '+ aktifResim +' / '+ resimSayisi +' ' );
}
/*Surukle Yukle*/
function resimYukleme(){

    if ($('.dropzone1').size() > 0) {

        Dropzone.autoDiscover = false;
        $( ".dropzone1" ).dropzone({
            acceptedFiles: "image/jpeg",
            url: 'upload.php',
            maxFiles: 1, // yuklenecek dosya siniri
            maxFilesize: 1, // yuklenecek dosyanin max mb boyutu
            init: function() {

                var benimDropzone = this;

                //dropzone icini temizleme
                $( '.resimYukle' ).click(function(event) {
                  benimDropzone.removeAllFiles(true);
                });
                
                this.on( "addedfile", function(file, response) {
                    pop1.close();
                    $( '#eklenenDosya' ).empty();
                    $( '.ilkYorum textarea' ).addClass( 'dosyaEklendi' );
                    // resmi 64 bit e cevirme
                    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
                        var reader = new FileReader(file);
                        reader.readAsDataURL(file);
                        reader.onload = function(e) {
                            var data = e.target.result,
                            $img = $( '<img />' ).attr( 'src', data).fadeIn();
                            var resim = $img
                            //resmi div e ekleme
                            $( '#eklenenDosya' ).html(resim).append( '<a class="ekliDosyayiKaldir" href="#"><i class="fa fa-times-circle"></i></a>' );
                            $( '.ekliDosyayiKaldir' ).click(function(event) {
                                event.preventDefault();
                                $( '#eklenenDosya' ).empty();
                                $( '.ilkYorum textarea' ).removeClass( 'dosyaEklendi' );
                            });
                        };
                    }
                });
                
                //Gonderme basarili ise dropzone icini temizle
                this.on( "success", function(file, response) {
                    benimDropzone.removeAllFiles(true);
                });

                this.on( "error", function(file, response) {
                  //hata bolumu
                });
            }
        });
    }  
}

function resimYukleme2(){
     $(".mesajAlani .nano").nanoScroller({ scrollTop: $('.mesajAlani ul').outerHeight() });

    if ($('.dropzone2').size() > 0) {
        
        Dropzone.autoDiscover = false;

        $( ".dropzone2" ).dropzone({
            acceptedFiles: "image/jpeg",
            url: 'upload.php',
            maxFiles: 10, // yuklenecek dosya siniri
            maxFilesize:  2, // yuklenecek dosyanin max mb boyutu

            init: function() {
                 dz2 = this;
                 var count= dz2.files.length;
                //dropzone icini temizleme
              
                /*mesaj resim yukleme alani ac*/
                $('.mesajPopTutucu .mesajicerik .mesajGonder .contentWrapper .ekle a').click(function() {
                    $('.mesajicerik .mesajAlani .nano').finish().fadeToggle(250,function(){$('#resimYukle2').finish().fadeToggle(250);}); 
                });
                count = 0;
                 this.on( "addedfile", function(file, response) {
                    count = this.files.length;
                    $( '.dosyaEkle .imgWrapper' ).addClass( 'eklendi' );
                    // resmi 64 bit e cevirme
                    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
                        var reader = new FileReader(file);
                        reader.readAsDataURL(file);
                        reader.onload = function(e) {
                            var data = e.target.result,
                            $img = $( '<img />' ).attr( 'src', data);
                            //resmi div e ekleme
                            $( '.dosyaEkle .imgWrapper' ).append(
                                $('<div/>').addClass('imgWrap').append($img).append(
                                    $('<a href="javascript:void(0)"><i class="fa fa-times" aria-hidden="true"></i></a>').click(function(){
                                        if($(this).closest('.imgWrapper ').find('.imgWrap').size() > 1)
                                        {
                                            
                                        } else {
                                            $('.dosyaEkle .imgWrapper').removeClass('eklendi');    
                                        }   
                                        $(this).parent().remove();
                                    })
                                )
                            );
                         
                        };
                    }
                });

                count2 = 0;
                this.on( "complete", function(file) {
                    count2++;
                    dz2.removeFile(file);
                    if(count == count2)
                    {
                    }
                });


           
                //Gonderme basarili ise dropzone icini temizle
                this.on( "thumbnail", function(file) {
                });

                this.on( "error", function(file, response) {
                  //hata bolumu
                });

            }
        });
    };
}

function resimYukleme3(){
    $(".mesajAlani .nano").nanoScroller({ scrollTop: $('.mesajAlani ul').outerHeight() });

    if ($('.dropzone3').size() > 0) {
        
        Dropzone.autoDiscover = false;

        $( ".dropzone3" ).dropzone({
            acceptedFiles: "image/jpeg",
            url: 'upload.php',
            maxFiles: 10, // yuklenecek dosya siniri
            maxFilesize:  2, // yuklenecek dosyanin max mb boyutu

            init: function() {
                 dz = this;
                 var count= dz.files.length;
                //dropzone icini temizleme
              
                /*mesaj resim yukleme alani ac*/
                $('.mesajicerik .mesajGonder .contentWrapper .ekle a').click(function() {
                    $('.mesajWrapper .mesajicerik .mesajAlani .nano').finish().fadeToggle(250,function(){$('#resimYukle2').finish().fadeToggle(250);}); 
                });
                count = 0;
                 this.on( "addedfile", function(file, response) {
                    count = this.files.length;
                    $( '.dosyaEkle .imgWrapper' ).addClass( 'eklendi' );
                    // resmi 64 bit e cevirme
                    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
                        var reader = new FileReader(file);
                        reader.readAsDataURL(file);
                        reader.onload = function(e) {
                            var data = e.target.result,
                            $img = $( '<img />' ).attr( 'src', data);
                            //resmi div e ekleme
                            $( '.dosyaEkle .imgWrapper' ).append(
                                $('<div/>').addClass('imgWrap').append($img).append(
                                    $('<a href="javascript:void(0)"><i class="fa fa-times" aria-hidden="true"></i></a>').click(function(){
                                        if($(this).closest('.imgWrapper ').find('.imgWrap').size() > 1)
                                        {
                                            
                                        } else {
                                            $('.dosyaEkle .imgWrapper').removeClass('eklendi');    
                                        }   
                                        $(this).parent().remove();
                                    })
                                )
                            );
                         
                        };
                    }
                });

                count2 = 0;
                this.on( "complete", function(file) {
                    count2++;
                    dz.removeFile(file);
                    if(count == count2)
                    {
                    }
                });


           
                //Gonderme basarili ise dropzone icini temizle
                this.on( "thumbnail", function(file) {
                });

                this.on( "error", function(file, response) {
                  //hata bolumu
                });

            }
        });
    };
}


/*Yorum Bildirme*/
function yorumBildir2(){
    $( '.yorumuBildir input[type="submit"]' ).click(function(event) {
        var isim = jQuery.trim($( this ).closest( '.yorumuBildir' ).find( '.isimInput' ).val());
        var eposta = jQuery.trim($( this ).closest( '.yorumuBildir' ).find( '.epostaInput' ).val());
        var yorum = jQuery.trim($( this ).closest( '.yorumuBildir' ).find( 'textarea' ).val());
        
        if (isim == '' || eposta == '' || yorum == '' ) {
            alert( 'boş alanlar var..' );
        } else {
            $( this ).closest( '.yorumuBildir' ).find( 'textarea' ).attr( 'disabled', 'disabled' );
            $( this ).closest( '.yorumuBildir' ).find( '.yorumGonder' ).html( '<span class="bildirimUlasti"><i class="fa fa-check"></i> Bildiriminiz bize ulaştı. En kısa zamanda yorum hakkında işlem yapılacaktır.</span>' )
        };
    });
}

/*Ilk yorum gonderme*/
function ilkYorumGonderme(){
    $( '.yorumYaz.ilkYorum .yorumGonderButton' ).click(function(event) {
        var isim = jQuery.trim($( this ).closest( '.yorumYaz.ilkYorum' ).find( '.yorumGonder input[type="text"]' ).val());
        var yorum = jQuery.trim($( this ).closest( '.yorumYaz.ilkYorum' ).find( 'textarea' ).val());
        
        if (isim == '' || yorum == '' ) {
            alert( 'boş alanlar var..' );
        } else {
            //eklenen dosyayi temizleme ve textarea yi normal haline cevirme
            $( '#eklenenDosya' ).empty();
            $( this ).closest( '.yorumYaz.ilkYorum' ).find( '.yorum' ).removeClass( 'dosyaEklendi' );
            //inputlari temizleme
            $( this ).closest( '.yorumYaz.ilkYorum' ).find( '.yorumGonder input[type="text"]' ).val( '' );
            $( this ).closest( '.yorumYaz.ilkYorum' ).find( 'textarea' ).val( '' );
            //yorum siniri tekrar 1000 yapma
            $( this ).closest( '.yorumYaz.ilkYorum' ).find( '.yorumSinir' ).html(1000);
            //pop acilacak
            pop2.open();
        };
    });
}

/*Yoruma cevap yazma*/
function yorumaCevapYaz(){
    $( '.yorumYaz' ).not( '.ilkYorum' ).find( 'input[type="submit"]' ).click(function(event) {
        var isim = jQuery.trim($( this ).closest( '.yorumYaz' ).find( '.yorumGonder input[type="text"]' ).val());
        var yorum = jQuery.trim($( this ).closest( '.yorumYaz' ).find( 'textarea' ).val());
        
        if (isim == '' || yorum == '' ) {
            alert( 'boş alanlar var..' );
        } else {
            //inputlari temizleme
            $( this ).closest( '.yorumYaz' ).find( '.yorumGonder input[type="text"]' ).val( '' );
            $( this ).closest( '.yorumYaz' ).find( 'textarea' ).val( '' );
            //pop acilacak
            pop2.open();
        };
    });
}


/*Sitene Ekle Kopyalandi Butonu*/
function siteneEkleKopyalama(){
    $( '.siteneEkleSag .kopyala' ).click(function(event) {
        event.preventDefault();
        $( this ).html( '<i class="fa fa-check"></i> Kopyalandı' );
    });
}
/*Yazi Kopyalama*/
function copyToClipboard(element) {
  var $temp = $( "<input>" );
  $( "body" ).append($temp);
  //$(element).val() value yi kopyalar $(element).text() yaziyi kopyalar
  $temp.val($(element).val()).select();
  document.execCommand( "copy" );
  $temp.remove();
}


/*Okunmamis bildirimi okundu isaretleme*/
function okunmamisBildirimler(){
    $( '.profilAyarlari .detay4 ul li' ).click(function(event) {
        $( this ).removeClass( 'okunmamis' );
    });
}

/*Header Profile Menu Acma*/
function headerProfile(){
    $( '.uyePanel2 .profilMenu' ).click(function(event) {
        event.preventDefault();
        hedefDisiTiklama(['.profilMenu, .profilAcilirMenu'],function(){
            $( '.profilAcilirMenu ul' ).fadeOut();
        }); 
        $( this ).closest( '.uyePanel2' ).find( '.profilAcilirMenu ul' ).finish().fadeToggle();
    });
}

/*Uye Bildir Menu Acma*/
function uyeBildirMenu(){
    $( '.takipEtTutucu .bildirEngelleButon' ).click(function(event) {
        event.preventDefault();
        hedefDisiTiklama(['.bildirEngelleButon, .engelleBildirMenu'],function(){
            $( '.engelleBildirMenu' ).fadeOut();
        }); 
        $( this ).closest( '.takipEtTutucu' ).find( '.engelleBildirMenu' ).finish().fadeToggle();
    });
}

/*Uyeyi bildir butonu*/
function uyeBildirildi(){

    $( '.uyeBildirTututucu .gonder' ).click(function(event) {
        var value = jQuery.trim($( '.uyeBildirTututucu textarea' ).val());
        if (value == '' ) {
            alert( 'boş alanlar var' );
        } else {
            $( '.uyeBildirTututucu' ).fadeOut(0);
            $( '.uyeBildirildi' ).fadeIn();
        }
    });
}

/*Kullanici Menusu Profil Menusu*/
function kullaniciMenusu(){
    $( '.kullaniciMenusu .profilMenu .acilirMenu > a' ).click(function(event) {
        event.preventDefault();
        if ($( this ).closest( '.acilirMenu' ).find( '.ayarlarMenusu ul li a' ).hasClass( 'secili' )){

        } else { 
            if ($( this ).closest( '.acilirMenu' ).hasClass( 'acik' )) {
                $( this ).closest( '.acilirMenu' ).removeClass( 'acik' );
                $( this ).removeClass( 'secili' );
            } else {
                $( this ).closest( '.acilirMenu' ).addClass( 'acik' );
                $( this ).addClass( 'secili' );
            }
        }
    });
}
/*Kullanici Menusu Profil Menusu*/
function acilirMenu2(){
    $( 'li.acilirMenu2 > a' ).click(function(event) {
        event.preventDefault();
        if ($( this ).closest( 'li.acilirMenu2' ).hasClass( 'secili' )) {
            $( this ).closest('li.acilirMenu2').removeClass( 'secili' );
        } else {
            $( this ).closest('li.acilirMenu2').addClass( 'secili' );
        }
    });
    hedefDisiTiklama(
            ['li.acilirMenu2','li.acilirMenu2 *'],
        function(){
            $('li.acilirMenu2').removeClass( 'secili' );
        }
    );
}

$('.mesajAc').click(function() {

    if(!$(this).hasClass('active'))
    {
        $(this).addClass('active');
        $(this).parent().find('.mesajAcilirWrapper').finish().fadeIn(250,function(){
             $(".mesajPop  .mesajAcilirWrapper .nano").nanoScroller({
              sliderMaxHeight: 120,
              alwaysVisible: true
            });
        });
  
    } else {
        $(this).removeClass('active');
        $(this).parent().find('.mesajAcilirWrapper').fadeOut(250,function(){
             $(".mesajPop  .mesajAcilirWrapper .nano").nanoScroller({ destroy: true });
        });
    }

    hedefDisiTiklama(['.mesajPop .mesajAc','.mesajPop .mesajAcilirWrapper'],
        function(){
            if(!$('.mesajPop  .mesajAc').hasClass('active'))
            {
                return;
            }
            $(".mesajPop .mesajAcilirWrapper").fadeOut(0,function(){
            $(".mesajPop .mesajAcilirWrapper .nano").nanoScroller({ destroy: true });
            $('.mesajPop .mesajAc').removeClass('active');
        });
    });
});

$('.bildirimAc').click(function() {

    if(!$(this).hasClass('active'))
    {
        $(this).addClass('active');
        $(this).parent().find('.bildirimAcilirWrapper').fadeIn(250,function(){
            $(".bilidirimPop .bildirimAcilirWrapper .nano").nanoScroller({
                sliderMaxHeight: 120,
                alwaysVisible: true
            });
        });
    } else {
        $(this).removeClass('active');
        $(this).parent().find('.bildirimAcilirWrapper').finish() .fadeOut(250,function(){
             $(".bilidirimPop .bildirimAcilirWrapper .nano").nanoScroller({ destroy: true });
        });
    }

    hedefDisiTiklama(['.bilidirimPop .bildirimAc','.bilidirimPop .bildirimAcilirWrapper'],
        function(){
            if(!$('.bilidirimPop .bildirimAc').hasClass('active'))
            {
                return;
            }
            $(".bilidirimPop  .bildirimAcilirWrapper").fadeOut(0,function(){
            $(".bilidirimPop .bildirimAcilirWrapper .nano").nanoScroller({ destroy: true });
            $('.bilidirimPop .bildirimAc').removeClass('active');
        });
    });
});


/*Biyografi devamini gor*/

$('.biyografiDetayBilgiler .devaminiGor').click(function() {
    $(this).css('display','none');
    $('.biyografiDetayBilgiler .acilirAlan').slideToggle(800);
});

/*Mesaj smile ac*/
$('.smile').click(function() {
    hedefDisiTiklama(['.smile'],function(){
        $( '.mesajicerik .mesajGonder .content ul').fadeOut();
    });
    $('.mesajicerik .mesajGonder .content ul').finish().fadeToggle();
});

 $(document).bind('keydown',function(e){
    if(e.keyCode === 13 && $.trim($('.contentWrapper input').val()).length > 0)
    {

        $('.mesajicerik .mesajGonder .content a.gonder').trigger('click');
    }
});

/* Konusma var ise mesajyok'u sildiriyoruz*/

    if($('.mesajAlani ul li.mesaj').size() > 0)
    {
        $('.mesajAlani ul li.mesajYok').remove();
    }

/*Emoji Gönder*/
$('.emoji li a img').on('click',function(e){
    e.preventDefault();
    var emoji = $(this);
    $('.contentWrapper input').val($('.contentWrapper input').val() + emoji.attr('data-code'));
    $('.mesajicerik .mesajGonder .contentWrapper input').trigger('focus');
//    $('ul.emoji').fadeOut(0);
});

/*Mesaj gonder*/
$('.mesajicerik .mesajGonder .content a.gonder').click(function(e) {
    e.preventDefault();
    var mesaj = $.trim($('.contentWrapper input').val());

    if(mesaj.length < 1 && $(this).closest('.mesajicerik').find('.imgWrap').size() < 1)
    {        
        return;
    }

    $('.contentWrapper input').val('').focus();
    var img = $('#user2').val();
    var DOM = '';
    DOM +='<li class="mesaj kullanici2">';
        DOM +='<a href="#"><img class="profil" src="'+img+'"></a>';
        DOM +='<p>'+getirResim($(this)) +emojiOlustur(mesaj)+'</p>';
    DOM +='</li>';

    $('.mesajAlani .nano-content ul').append(DOM);
    $(".mesajAlani .nano").nanoScroller({ destroy: true });
    $(".mesajAlani .nano").nanoScroller({
        sliderMaxHeight: 56,
        alwaysVisible: true
    });

    $(".mesajAlani .nano").nanoScroller({ scrollTop: $('.mesajAlani ul').outerHeight()});
    $(this).closest('.mesajicerik').find('.imgWrapper ').empty();
    $(this).closest('.mesajicerik').find('.imgWrapper').removeClass('eklendi');

    $('#resimYukle2').fadeOut(250,function(){
        $(this).parent().find('.nano ').fadeIn(250);
    });
});

/*mesaj resim yukleme alani kapat*/
$('.resimYukleKapat').click(function() {
    $(this).closest('#resimYukle2').fadeOut();
});

function getirResim(obj)
{
    var DOM = '';
    obj.closest('.mesajicerik').find('.imgWrap').each(function(){
        DOM += '<a href="'+$(this).find('img').attr('src')+'"rel="fancyboxGalery" class="fancybox"><img src="'+$(this).find('img').attr('src')+'"/></a>';
    });
    return DOM;

}
function emojiOlustur(mesaj){
    return mesaj.replace(/:D/g, '<img src="assets/thumb/smile/smile1.png" class="emojii"/>')
                .replace(/:A/g, '<img src="assets/thumb/smile/smile2.png" class="emojii"/>')
                .replace(/:B/g, '<img src="assets/thumb/smile/smile3.png" class="emojii"/>')
                .replace(/:C/g, '<img src="assets/thumb/smile/smile4.png" class="emojii"/>')
                .replace(/:D/g, '<img src="assets/thumb/smile/smile5.png" class="emojii"/>')
                .replace(/:E/g, '<img src="assets/thumb/smile/smile6.png" class="emojii"/>')
                .replace(/:F/g, '<img src="assets/thumb/smile/smile7.png" class="emojii"/>')
                .replace(/:G/g, '<img src="assets/thumb/smile/smile8.png" class="emojii"/>')
                .replace(/:H/g, '<img src="assets/thumb/smile/smile9.png" class="emojii"/>')
                .replace(/:K/g, '<img src="assets/thumb/smile/smile10.png" class="emojii"/>')
                .replace(/:L/g, '<img src="assets/thumb/smile/smile11.png" class="emojii"/>')
                .replace(/:M/g, '<img src="assets/thumb/smile/smile12.png" class="emojii"/>')
                .replace(/:N/g, '<img src="assets/thumb/smile/smile13.png" class="emojii"/>')
                .replace(/:O/g, '<img src="assets/thumb/smile/smile14.png" class="emojii"/>')
                .replace(/:P/g, '<img src="assets/thumb/smile/smile15.png" class="emojii"/>')
                .replace(/:R/g, '<img src="assets/thumb/smile/smile16.png" class="emojii"/>')
                .replace(/:S/g, '<img src="assets/thumb/smile/smile17.png" class="emojii"/>')
                .replace(/:T/g, '<img src="assets/thumb/smile/smile18.png" class="emojii"/>')
                .replace(/:V/g, '<img src="assets/thumb/smile/smile19.png" class="emojii"/>')
                .replace(/:Y/g, '<img src="assets/thumb/smile/smile20.png" class="emojii"/>')
                .replace(/:Z/g, '<img src="assets/thumb/smile/smile21.png" class="emojii"/>')
                .replace(/:a/g, '<img src="assets/thumb/smile/smile22.png" class="emojii"/>')
                .replace(/:b/g, '<img src="assets/thumb/smile/smile23.png" class="emojii"/>')
                .replace(/:c/g, '<img src="assets/thumb/smile/smile24.png" class="emojii"/>')
                .replace(/:d/g, '<img src="assets/thumb/smile/smile25.png" class="emojii"/>')
                .replace(/:e/g, '<img src="assets/thumb/smile/smile26.png" class="emojii"/>')
                .replace(/:f/g, '<img src="assets/thumb/smile/smile27.png" class="emojii"/>')
                .replace(/:g/g, '<img src="assets/thumb/smile/smile28.png" class="emojii"/>')
}
/*engelle acilir menu*/
$('.kullaniciAdi .secenek').click(function() {

    hedefDisiTiklama(['.kullaniciAdi .secenek','.mesajWrapper .mesajicerik .kullaniciAdi .menu'],function(){
        $( '.mesajWrapper .mesajicerik .kullaniciAdi .menu' ).fadeOut(250);
    });
    
    $('.mesajWrapper .mesajicerik .kullaniciAdi .menu').fadeToggle();
});


/*Mesaj Pop*/
function mesajPop(){
    mesajPop =  $( '#mesaj' ).simplePop();
    setTimeout(function(){
        $(".nano").nanoScroller({
          sliderMaxHeight: 56,
          alwaysVisible: true
        });
    }, 300);
    $( '.mesajPopKapat' ).click(function(){
        mesajPop.close();
    });
}
/*Uye Girisi Pop*/
function uyeGirisiPop(){
    uyeGirisiPop =  $( '#girisYap' ).simplePop();
    $( '.uyeGirisiPopKapat' ).click(function(){
        uyeGirisiPop.close();
    });
}
/*Uye girisi veya Uye Ol*/
function uyeGirisi(){
    $( '.girisPopup .uyelikOlusturButon' ).click(function(event) {
        if($( '.girisBolumu' ).hasClass( 'acik' )) {

            $( '.girisPopup .girisBolumu' ).finish().hide(0);
            $( '.girisPopup .uyeOlBolumu' ).finish().fadeIn(500);

            $( '.girisPopup .sifreSifirla' ).finish().hide(0);
            $( '.girisPopup .hataliEposta' ).finish().hide(0);
            $( '.girisPopup .sifrenizGonderildi' ).finish().hide(0);

            $( '.girisBolumu' ).removeClass( 'acik' );
            $( '.uyeOlBolumu' ).addClass( 'acik' );
            $( this ).html( 'Üye misin? <strong>Giriş Yap</strong>' );

        } else {
            
            $( '.girisPopup .uyeOlBolumu' ).finish().hide(0);
            $( '.girisPopup .girisBolumu' ).finish().fadeIn(500);

            $( '.girisBolumu' ).addClass( 'acik' );
            $( '.uyeOlBolumu' ).removeClass( 'acik' );
            $( this ).html( 'Yeni bir üyelik oluşturun' );
        }
    });

    /*Sifremi Unuttum Butonu*/
    $( '.sifreUnuttum' ).click(function(event) {
        $( '.girisBolumu' ).hide(0);
        $( '.girisPopup .girisBolumu' ).finish().hide(0);
        $( '.girisPopup .sifreSifirla' ).finish().fadeIn(500);
    });

    /*Sifre Sifirlama*/
    $( '.sifreSifirla input[type="submit"]' ).click(function(event) {
        var input = jQuery.trim($( '.sifreSifirla input[type="text"]' ).val());
        if (input == '' ) {
            $( '.girisPopup .sifreSifirla' ).finish().hide(0);
            $( '.girisPopup .hataliEposta' ).finish().fadeIn(500);
        } else {
            $( '.girisPopup .sifreSifirla' ).finish().hide(0);
            $( '.girisPopup .sifrenizGonderildi' ).finish().fadeIn(500);
        };
    });

    /*Sifre Sifirlama Tekrar Dene*/
    $( '.girisPopup .tekrarDene' ).click(function(event) {
        $( '.girisPopup .hataliEposta' ).finish().hide(0);
        $( '.girisPopup .sifreSifirla' ).finish().fadeIn(500);
    });
}

/*Yorum Gonderildi Pop Icindeki Buton*/
$('.popUyeOlButon').click(function(event) {
    event.preventDefault();
    pop2.close(function(){
        uyeGirisiPop.open();
    });
});


/*Avatar Secme*/
function avatarSecme() {
    $('.avatarSec').click(function(event) {
        event.preventDefault();
        uyeOlAdim2Pop.close(function(){
            avatarPop.open(function(){
                $(this).find('li a').on('click',function(){
                    var img = $(this).find('img').attr('src');
                    $('#uyeOlAdim2 .yuklenenResim img').attr('src', img);
                    avatarPop.close(function(){
                        uyeOlAdim2Pop.open();    
                    }); 
                })
            });    
        });
    });
    /*Avatar2 Secme*/
    $('.avatarSec2').click(function(event) {
        event.preventDefault();
        avatarPop.open(function(){
            $(this).find('li a').on('click',function(){
                var img = $(this).find('img').attr('src');
                avatarPop.close(); 
                $('.profilAyarlari .profilResmiYukle img').attr('src', img);
            })
        });    
    });
}


/*Resim Yukleme*/
$('#uyeOlAdim2 .file').change(function(){
    readURL(this);
});

$('.profilAyarlari .file').change(function(){
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.gelenResim').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}


/*Uyelik Ikinci Adima Gecme*/
function uyelikFormKontrolu() {   
    $('.girisPopup .uyeOlBolumu input[type="submit"]').click(function(event) {
        var isim = $('.uyeOlBolumu .isim').val();
        var eposta = $('.uyeOlBolumu .eposta').val();
        var sifre = $('.uyeOlBolumu .sifre').val();
        var sifre2 = $('.uyeOlBolumu .sifreTekrar').val();
        var sozlesme = $('.uyeOlBolumu .sozlesme input[type="checkbox"]').is(':checked');

        if (isim == '') {
            alert('isim boş..');
        } else if (eposta == '') {
            alert('eposta boş..');
        } else if (sifre == '') {
            alert('sifre boş..');
        } else if (sifre != sifre2) {
            alert('şifre ve şifre tekrarı uyuşmuyor..');
        } else if (sozlesme == false) {
            alert('sözleşme kabul edilmedi..');
        } else {
            uyeGirisiPop.close(function(){
                uyeOlAdim2Pop.open();    
            });
        };

    });
}

/*Harita*/
function harita() {
    //Sehir nufuslarina gore renk paleti olusturulmustur. Iptal icin values degerini silmeniz yeterli
    var sehir_nufus = {"1":"82700","2":"26480","3":"31530","4":"22820","5":"15550","6":"205790","7":"78830","8":"7860","9":"39940","10":"49210","11":"9120","12":"10690","13":"14100","14":"11890","15":"10520","16":"105260","17":"23240","18":"4750","19":"25350","20":"41100","21":"63430","22":"17890","23":"25570","24":"9650","25":"37350","26":"32300","27":"68240","28":"21140","29":"5430","30":"10700","31":"58560","32":"19460","33":"66610","34":"564890","35":"166200","36":"12820","37":"13950","38":"52910","39":"14710","40":"9610","41":"57790","42":"84160","43":"26490","44":"32500","45":"56550","46":"43550","47":"33240","48":"32510","49":"17550","50":"12250","51":"12670","52":"30760","53":"12740","54":"36390","55":"55280","56":"12230","57":"9010","58":"27230","59":"32800","60":"28180","61":"32970","62":"3760","63":"63760","64":"14550","65":"47470","66":"20700","67":"27350","68":"15470","69":"3700","70":"9600","71":"13350","72":"21840","73":"17440","74":"7470","75":"5050","76":"7670","77":"7340","78":"9310","79":"5240","80":"21350","81":"14580"};
    jQuery('#vmap').vectorMap({
        map: 'turkey_tr',
        enableZoom: true,
        showTooltip: true,
        backgroundColor: 'transparent',
        borderColor: '#fafafa',
        borderOpacity: 1,
        borderWidth: 0.7,
        color: '#e60000',
        hoverColor: '#b70000',
        onRegionClick: function(element, code, region) {
            var mesaj = 'Seçiminiz "'+ region + '" Plaka veya Numarası '+ code.toUpperCase();
            alert(mesaj);
        }
    });
}

/*Takip Et, Takibi Birak ve Engelle*/
function takipEt() {
    $('.takipEtTutucu.girisYapilmis .takipEt').click(function(event) {
        if($(this).hasClass('takipEdiliyor')) {
            $(this).removeClass('takipEdiliyor');
            $(this).html('<i class="fa fa-plus"></i> Takip Et');
        } else if ($(this).hasClass('uyeEngellendi')) {
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.uyeyiEngelle').html('Üyeyi Engelle');
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').removeClass('uyeEngellendi');
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').html('<i class="fa fa-plus"></i> Takip Et');
        } else {
            $(this).addClass('takipEdiliyor');
            $(this).html('<i class="fa fa-times"></i> Takibi Bırak');
        }
    });

    $('.takipEtTutucu.girisYapilmis .uyeyiEngelle').click(function(event) {
        if($(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').hasClass('uyeEngellendi')) {
            $(this).html('Üyeyi Engelle');
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').removeClass('uyeEngellendi');
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').html('<i class="fa fa-plus"></i> Takip Et');
        } else {
            $(this).html('Engellemeyi Kaldır');
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').removeClass('takipEdiliyor');
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').addClass('uyeEngellendi');
            $(this).closest('.takipEtTutucu.girisYapilmis').find('.takipEt').html('<i class="fa fa-user-times"></i> Engeli Kaldır');
        }
    });
}


/*Anasayfa gazete Slideri*/
function masterSlider() {  
    var slider = new MasterSlider();
    slider.control('arrows' , {autohide:false, insertTo:'#gazetelerSliderYon'});
    slider.setup('masterslider' , {
        width     : 225,
        height    : 345,
        loop      : true,
        view      : 'flow',
        space     : -80,
        fullscreenMargin: 43,
        layout    : 'partialview',
        speed     : 50,
        autoplay : true,
        filters: {
            grayscale  : 1,
            contrast   : 1,
            opacity: 0.9
        }
    });
}


/*Yapiskan Menu*/
$(window).scroll(function(){
    if($(document).scrollTop() > 200){

      $('.headerUstTutucu').addClass('fixedMenu');
      $('header.ust').addClass('ziplatma');
      $('.yukariZipla').fadeIn(500)
    } else{

      $('.headerUstTutucu').removeClass('fixedMenu');
      $('header.ust').removeClass('ziplatma');
      $('.yukariZipla').fadeOut(500)
    }
});

/*insan kaynaklari cv ekleme*/
function cvFile() {
    $('.cvFile').change(function(){
        var val = $(this).val();
        $(this).closest('.inputTutucu').find('input[type="text"]').val(val);
    });
}

/*Nano Scroll*/
function nano() {
    $(".nano").nanoScroller();
}

/*arama yapinca aramayi acma*/
function aramaBolumu() {

    $('.aramaBolumu input').focus(function(event) {
        $(this).closest('.aramaBolumu').addClass('aktif');
    });

    $('.aramaBolumu input').keyup(function(event) {
        value = $('.aramaBolumu input').val();
        if (value.length > 2) {
            $('body').addClass('lockScroll');
            $('.aramaDetayBolumu').slideDown(function(){
                $(".aramaBolumuSag .nano").nanoScroller();
                var height =  $(window).outerHeight() - $('.aramaBolumuSag').offset().top;
                $(".aramaBolumuSag .nano").height(height);
                $( ".aramaDetayBolumu img.lazy" ).lazyload({
                    effect : "fadeIn"
                });
            });
        };
    });

    $('.aramaBolumu .aramaKapat').click(function(event) {
        $('.aramaDetayBolumu').slideUp();
        $('.aramaBolumu').removeClass('aktif');
        $('body').removeClass('lockScroll');
    });

    $('.aramaBolumu .fa-times').click(function(event) {
        $(this).closest('.inputTutucu').find('input').val('');
    });
}

/*yorum acilir menu*/

$('.yorumMenu .isim').click(function(e) {
    e.preventDefault();
    if($(this).parent().find('.menu').size() > 0)
        return;

    $('.yorumMenu .menu').fadeOut(0,
        function(){
            $('.yorumMenu .menu').remove();
        }
    );

    var DOM =  '';
    DOM += '<ul class="menu">';
    DOM += '    <li><a href="'+$(this).attr('href')+'">Profili Git</a></li>';
    DOM += '    <li><a href="javascript:void(0);" onclick="mesajPop.open();">Mesaj Gönder</a></li>';
    DOM += '    <li><a href="javascript:void(0);">Üyeyi Engelle</a></li>';
    DOM += '</ul>';

  
    $(this).closest('.yorumMenu').append(DOM);
    $(this).closest('.yorumMenu').find('.menu').fadeIn();

    hedefDisiTiklama(['.yorumMenu'],function(){
        $('.yorumMenu .menu').fadeOut(0,function(){
            $('.yorumMenu .menu').remove();
        })

    });
    
});

/*Galeri sayfasi yon isaretleri*/
function galeriYon() {
    if ($('.galeriDetay .galeriButonlar').size() > 0) {
        var seciliResim = $('.galeriButonlar .butonlar a.aktif').index();
        //Sol Tus
        $(document).bind( 'keydown', function (e){
            if(e.keyCode === 37) {
                var oncekiLink = $('.galeriButonlar .butonlar a').eq(seciliResim-1).attr('href');
                alert('açılacak önceki link:'+oncekiLink)
            }
        });
        //Sag Tus
        $(document).bind( 'keydown', function (e){
            if(e.keyCode === 39) {
                var sonrakiLink = $('.galeriButonlar .butonlar a').eq(seciliResim+1).attr('href');
                alert('açılacak sonraki link:'+sonrakiLink)
            }
        });
    };
}
/* verticalScrolling */
verticalScrolling();

$( ".select2" ).change(function() {
  $(this).addClass('active');
});