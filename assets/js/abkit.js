;(function($)
{
    $.fn.abCheckbox = function ( options )
    {   
        var defaults = {
            onClick      : function (){},
            onChange     : function (){}
        };
        
        $.extend(defaults,  options);
        
        $(this).not(".ab-checkbox-modified").each(function ()
        {
            var el = $(this);
            
            el.addClass('ab-checkbox-modified');
            var abChecked = el.prop("checked") ? "ab-checked" : "";

            // checkbox and click event creator 
            el.wrap(  "<div class='ab-checkbox-container'/>" );
            el.closest('.ab-checkbox-container').append( "<a href='#' class='ab-checkbox-trigger " + abChecked + "'></a>");
                
    
            if( el.closest('.ab-checkbox-container').parent().is('label'))
            {
                el.closest('label').click
                (
                    function (e)
                    {
                        e.preventDefault();
                        var input = $( this ).find( "input[type=checkbox]" );
                        var el_A  = $( this ).find( "a.ab-checkbox-trigger" );
                        islem(el_A,input);
                    }
                );
            } else {
                el.parent().find('a.ab-checkbox-trigger').click
                ( 
                    function (e)
                    {   
                        e.preventDefault();
                        var input = $( this ).closest( '.ab-checkbox-container' ).find( "input[type=checkbox]" );
                        var el_A  = $( this );
                        islem(el_A,input);
                    }
                );
            }

            // disardan etkilenmesi icin
            el.bind('change',function()
            {
                var input = $(this);
                var el_A = $( this ).closest('.ab-checkbox-container').find( "a.ab-checkbox-trigger" );
                
                defaults.onClick(input);
                switch ( input.prop( "checked" ) )
                {
                    case true:
                        el_A.addClass( 'ab-checked' );
                    break;
                    
                    case false:
                        el_A.removeClass( 'ab-checked' );
                    break;
                }
                defaults.onChange(input);
            });

            function islem(a,input)
            {
                defaults.onClick(input);
                if( input.prop( "checked" ) )
                {
                   // un checked
                   input.prop( "checked" ,false );
                   a.removeClass( 'ab-checked' );
                } else {
                   // checked
                   input.prop( "checked" ,true);
                   a.addClass( 'ab-checked' );
                }
                defaults.onChange(input);
            }
        });
        
        this.destroy = function () 
        {
            $(this).each(function ()
            {
                $(this).removeClass('ab-checkbox-modified').parent().find('a').remove();
                if( $(this).parent().is('.ab-checkbox-container') )
                    $(this).unwrap();
                $(this).unbind("click");
                $(this).unbind("change");
                $( this ).closest( 'label' ).unbind("click");
                $( this ).closest( 'label' ).unbind("change");
            });
        };
        
        // returns the current jQuery object
        return this;
    };
    
    $.fn.abRadio = function ( options )
    {   
        var defaults = {
            onClick      : function (){},
            onChange     : function (){}
        };
        
        $.extend(defaults,  options);
        
        $(this).not(".ab-radio-modified").each(function ()
        {
            var el = $(this);
            
            el.addClass('ab-radio-modified');
            var abChecked = el.prop("checked") ? "ab-checked" : "";

            // radio and click event creator 
            el.wrap(  "<div class='ab-radio-container'/>" );
            el.closest('.ab-radio-container').append( "<a href='#' class='ab-radio-trigger " + abChecked + "'></a>");
                
    
            if( el.closest('.ab-radio-container').parent().is('label'))
            {
                el.closest('label').click
                (
                    function (e)
                    {
                        e.preventDefault();
                        var input = $( this ).find( "input[type=radio]" );
                        var el_A  = $( this ).find( "a.ab-radio-trigger" );
                        islem(el_A,input,el);
                    }
                );
            } else {
                el.parent().find('a.ab-radio-trigger').click
                ( 
                    function (e)
                    {   
                        e.preventDefault();
                        var input = $( this ).closest( '.ab-radio-container' ).find( "input[type=radio]" );
                        var el_A  = $( this );
                        islem(el_A,input,el);
                    }
                );
            }

            el.bind('change',function()
            {
                defaults.onClick(input);
                var input = $(this);
                var el_A = $( this ).closest('.ab-radio-container').find( "a.ab-radio-trigger" );
                $('.ab-radio-modified[name=' + input.attr('name') + ']').parent().find('a.ab-radio-trigger').removeClass('ab-checked');
                defaults.onChange(input);
                input.parent().find('a.ab-radio-trigger').addClass('ab-checked')
            });

            function islem(el_A,input,el)
            {   
                defaults.onClick(input);
                $('.ab-radio-modified[name=' + input.attr('name') + ']').parent().find('a.ab-radio-trigger').removeClass('ab-checked');
                input.prop( "checked" , true );
                el_A.addClass( 'ab-checked' );
                defaults.onChange(input);
            }
        });
        
        this.destroy = function () 
        {
            $(this).each(function ()
            {
                $(this).removeClass('ab-radio-modified').parent().find('a').remove();
                if( $(this).parent().is('.ab-radio-container') )
                    $(this).unwrap();
                $(this).unbind("click");
                $(this).unbind("change");
                $( this ).closest( 'label' ).unbind("click");
                $( this ).closest( 'label' ).unbind("change");
            });
        };
        
        // returns the current jQuery object
        return this;
    };
        
    /***********************************************/
    /* simplePop                                   */
    /***********************************************/
   
    $.fn.simplePop = function (options )
    {   
        var  defaults = 
        {   
            // Callback
            open         : function (){},
            close        : function (){},
            beforeClose  : function (){},
            // Close
            closeEsc     : true,
            closeTag     : ".simplePopClose",
            closeBg      : true,
            // align valing
            resize       : true,
            // Speed
            openSpeed    : 250,
            closeSpeed   : 200,
            // Style
            background   : '#000',
            bgOpacity    : 0.80,
            //Auto
            auto         : false,
            autoDelay    : 0
        };
        
        var popup   = {};
        var el      = this;
        
        if( this.length == 0) return this;

        // support mutltiple elements
        if( this.length > 1){
            this.each(function(){$( this ).simplePopup(options )});
            return this;
        }
        
        var init = function()
        {

            popup = $.extend({}, defaults, options );

            // hide target popup
            var target = el;
            target.hide(0);
            
            // Auto Open
            if( popup.auto )
            {
                setTimeout(function () 
                {
                    el.simplePopOpen( target ); 
                }, popup.openSpeed );
            }
         
            if( popup.resize )
            {
                $( window ).resize(function()
                {
                    if( $( '.simplePopBg' ).size() > 0 )
                    {
                        $( '.simplePopBg' ).css(
                        {
                            width       : $( window ).outerWidth(),
                            height      : $( window ).outerHeight()
                        });
                    }
                });
            }
        }
        
//      el.create();
        
        el.simplePopOpen =  function ( target, callback )
        {

            $('body').addClass('simpleOverFlow');
            $('.simplePopBg').remove();
            
            $( 'body' ).append
            (
                $( '<div class="simplePopBg"/>' )
                .css
                (
                    {
                        width       : $( window ).outerWidth(),
                        height      : $( window ).outerHeight(),
                        background  : popup.background,
                        position    : 'fixed',
                        left        : 0,
                        top         : 0,
                        zIndex      : 9988,
                        display     : 'none'
                    }
                )
                .fadeTo( popup.openSpeed, popup.bgOpacity )
                .fadeIn
                (
                    popup.openSpeed, 
                    function()
                    {
                        var marginTop  = target.outerHeight();
                        var top        = '50%';
                        var left       = target.outerWidth();

                        if( target.outerHeight() > $( window ).outerHeight())
                        {
                            marginTop = 20;
                            top       = 20;
                        }

                        target.css
                        (
                            {
                                zIndex      : 9990,
                                position    : 'fixed',
                                left        : '50%',
                                top         : top,
                                marginLeft  : - ( left / 2),
                                marginTop   : - ( marginTop / 2)
                            }
                        ).fadeIn
                        (
                            popup.openSpeed,
                            function()
                            {
                                target.addClass( 'simplePopActive' );
                                popup.open();
                                el.simplePopClose( target );
                                typeof callback === 'function' && callback.call(this);
                            }
                        );
                    }
                )
            );
        }

        el.simplePopClose = function( target )
        {
            if( popup.closeEsc )
            {
                $(document).bind( 'keydown', function (e){
                    if(e.keyCode === 27)
                        el.simpleCloseProces( target );
                });
            }

            if( popup.closeBg )
            {
                $( '.simplePopBg' ).click( function(){
                    el.simpleCloseProces( target );
                });
            }
        }

        el.simpleCloseProces = function( target, callback )
        {
            // before close
            popup.beforeClose();

            target.fadeOut( popup.closeSpeed,function(){
                $( '.simplePopBg' ).fadeOut( popup.closeSpeed, function(){ 
                    $( this ).remove();
                    popup.close();
                    $('body').removeClass('simpleOverFlow');
                    typeof callback === 'function' && callback.call(this);
                });   
            });
        }
        
        // Public Functions
        el.close = function(callback)
        {
            el.simpleCloseProces( el, callback );
            
        }
              
        el.open = function(callback)
        {
            el.simplePopOpen( el, callback );
        }

        init();

        // returns the current jQuery object
        return this;
    }

    /*
     * jQuery Tab Plugin (simpleTab)
     * Author   - @arayuzcomtr
     * Web      - http://www.arayuz.com.tr
     * Mail     - info@arayuz.com.tr
     * Version  - 0.3
    */

    $.fn.simpleTab = function ( options )
    {
        var  defaults = 
        {   
            activeTab       : 0,
            event           : 'click',
            activeClass     : 'active',
            tabEffect       : 'fadeIn' ,
            speed           : 250,
            auto            : false,
            autoSpeed       : 1000,
            specialTab      : false,
            nextArrow       : false,
            prevArrow       : false,
            tabLinkWrapper  : '> .tabLink',
            tabLinkATag     : '> .tabLink  a',
            tabContentX     : '> .tabContent',
            uniqInterval    : "tabTime",
            // Callback
            change          : function (){}
            
        };
        
        var tab   = {};
        var el      = this;
        
        // yoksa obje iptal
        if( this.length == 0) return this;

        // coklu calistirma
        if( this.length > 1){
            this.each(function(){$( this ).simpleTab( options )});
            return this;
        }
        
        // baslangic
        var init = function()
        {
            tab = $.extend({}, defaults, options );
            
            if(tab.specialTab)
            {
                tab.tabLink     = $(tab.tabLink);
                tab.tabLinkA    = $(tab.tabLinkA);
                tab.tabContent  = $(tab.tabContent);
            } else {
                tab.tabLink     = el.find('> .tabLink');
                tab.tabLinkA    = el.find('> .tabLink a ');
                tab.tabContent  = el.find('> .tabContent ');
            }
            
            
            el.simpleTabClose();
            
            if( tab.activeTab > 0 )
            {
                el.goToTab(tab.activeTab);
                // Nokta atisi
            } else {
                
                // Git birinciyi ac
                el.goToTab(0);
            }
            
            tab.tabLinkA.on( tab.event, function(e)
            {   
                e.preventDefault();
                el.goToTab( $(this).index() );
            });

           
            if(tab.auto)
            {
                autoTab();
                
                tab.tabLinkA.on({
                    mouseover :  function(e)
                    {   
                        e.preventDefault();
                        if(tab.auto)
                        {
                            clearInterval(tab.uniqInterval);
                        }    
                        el.goToTab( $(this).index() );
                    }, mouseout : function (){
                        autoTab();
                    }
                }
                );
            }      
            $(tab.nextArrow).on('click',function (e){
                clearInterval(tab.uniqInterval);
                e.preventDefault();
                activeTab =  tab.tabLink.find('.'+tab.activeClass).index();
                if((tab.tabLink.find('a').size() - 1) === activeTab)
                {
                } else {
                    el.goToTab(activeTab + 1 );  
                }
            });
            
            $(tab.prevArrow).on('click',function (e){
                clearInterval(tab.uniqInterval);
                e.preventDefault();
                activeTab =  tab.tabLink.find('.'+tab.activeClass).index();
                if(0 === activeTab)
                {
                } else {
                    el.goToTab(activeTab - 1 ); 
                }
            });
        }

        function autoTab(){
            tab.uniqInterval = setInterval(function(){
                activeTab =  tab.tabLink.find('.'+tab.activeClass).index();
                if((tab.tabLink.find('a').size() - 1) === activeTab)
                {
                    el.goToTab(0);
                } else {
                    el.goToTab(activeTab + 1 );  
                }
            },tab.autoSpeed);
        }
        
        el.simpleTabClose = function ()
        {
            tab.tabContent.hide( 0 );
            tab.tabLinkA.removeClass( tab.activeClass );
        }
        
        // Public Functions
        el.goToTab = function ( index )
        {
            tab.tabContent.stop(true,true);
            
            el.simpleTabClose();
            
            tab.tabLinkA.eq( index ).addClass( tab.activeClass );
            tab.tabContent.eq( index )[tab.tabEffect]( tab.speed, function (){
                tab.tabContent.eq( index ).addClass( tab.activeClass );
                tab.change(index);
            });
            
        }; 

        init();

        return this;
    }
        

    $.fn.abManset = function ( options )
    {   
        var defaults = {
            pSelector          : '',
            time               : 1500,
            autoTime           : 2000,
            gorsel             : '.mansetGorsel',
            gorselLink         : '.buyukGorsel',
            activeClass        : 'secili',
            intervalName       : 'abManset',
            /*public methods*/
            onClick      : function (){},
            onChange     : function (){}
        };
              
        var manset   = {};
        var el       = this;
        
        // yoksa obje iptal
        if( this.length == 0) return this;

        // coklu calistirma
        if( this.length > 1){
            this.each(function(){$( this ).abManset( options )});
            return this;
        }
        
        manset = $.extend({}, defaults, options );
        manset.paginationSelector  = el.find(' > ' + manset.pSelector );
        manset.pSelectorA = el.find(' > ' + manset.pSelector + ' > a' );
        manset.gorselImg           = el.find(' > ' + manset.gorsel + ' > ' + manset.gorselLink + ' > img ');
        manset.gorseLink           = el.find(' > ' + manset.gorsel + ' > ' + manset.gorselLink );
        manset.size =  manset.pSelectorA.size();
        // baslangic
        var init = function()
        {
            var index =  manset.paginationSelector.find('a.secili').index();
            manset.gorselImg.attr( 'src', manset.pSelectorA.eq(0).attr( 'data-buyuk-resim' ));
            manset.pSelectorA.removeClass( manset.activeClass );
            manset.pSelectorA.eq(0).addClass( manset.activeClass );

            if($.trim(manset.pSelectorA.eq(index).attr( 'data-title' )).length > 0)
            {
                if(el.find(manset.gorselLink).find('h2').size() > 0)
                {
                    el.find(manset.gorselLink).find('h2').html(manset.pSelectorA.eq(index).attr( 'data-title' ));
                } else {
                    el.find(manset.gorselLink).append('<h2>'+ manset.pSelectorA.eq(index).attr( 'data-title' ) + '</h2>');
                }
            }
            el.mansetCalistir();
            
        }

        el.mansetCalistir = function (){
            manset.intervalName = setInterval(function(){
                el.mansetler(  manset.paginationSelector.find('a.secili').index() );
            },manset.time);
        }

        el.mansetler = function(id){
            
            var index =  manset.paginationSelector.find('a.secili').index();

            if(index+1 == manset.size ){
                clearInterval(manset.intervalName);
                init();
            } else {
                manset.gorselImg.attr( 'src', manset.pSelectorA.eq(index+1).attr( 'data-buyuk-resim' ));
                manset.pSelectorA.removeClass( manset.activeClass );
                manset.pSelectorA.eq(index+1).addClass( manset.activeClass );

                if($.trim(manset.pSelectorA.eq(index+1).attr( 'data-title' )).length > 0)
                {
                    if(el.find(manset.gorselLink).find('h2').size() > 0)
                    {
                        el.find(manset.gorselLink).find('h2').html(manset.pSelectorA.eq(index+1).attr( 'data-title' ));
                    } else {
                        el.find(manset.gorselLink).append('<h2>'+ manset.pSelectorA.eq(index+1).attr( 'data-title' ) + '</h2>');
                    }
                }
            }
           
        }

        // Hover durumu
        manset.gorseLink.on({
            mouseover : function (){
                clearInterval(manset.intervalName);
            },
            mouseout : function(){
                clearInterval(manset.intervalName);
                el.mansetCalistir();
            }
        });
        // diger haberleri on izleme 
        manset.pSelectorA.on({
            mouseover : function (){
                var obj = $(this);
                clearInterval(manset.intervalName);
                
                manset.gorselImg.attr( 'src', obj.attr( 'data-buyuk-resim' ));
                
                manset.pSelectorA.removeClass( manset.activeClass );
                manset.pSelectorA.eq( obj.index() ).addClass( manset.activeClass );

                $(manset.gorselLink).attr('href',obj.attr( 'data-href' ));
                
                if($.trim(obj.attr( 'data-title' )).length > 0)
                {
                    if($(manset.gorselLink).find('h2').size() > 0)
                    {
                        $(manset.gorselLink).find('h2').html(obj.attr( 'data-title' ));
                    } else {
                        $(manset.gorselLink).append('<h2>'+ obj.attr( 'data-title' ) + '</h2>');
                    }
                }
//                $.cookie( manset.cookieName,$( this ).index());
            },
            mouseout : function (){
                clearInterval(manset.intervalName);
                el.mansetCalistir();
            }
        });
       
        init();
        return this;
    };

    
})(jQuery);

function hedefDisiTiklama(obje,callback){
    $( 'html' ).on( 'click', function(event){
        for(var i in obje){
            if ($(event.target).is(obje[i]) || $(event.target).parents(obje[i]).size() > 0){
                return;
            }   
        }
        callback();
    });
}

$('.mansetHaber').abManset({
    pSelector          : '.anaDigerMansetler',
    gorsel             : '.anaMansetGorsel1',
    intervalName       : "mansetZaman1",
    time               : 1500
});

$('.mansetHaber2').abManset({
    intervalName : "mansetZaman2",
    pSelector          : '.yanDigerMansetler',
    gorsel             : '.yanMansetGorsel1',
    time : 1000
});




var waitForFinalEvent = (function ()
{
    var timers = {};
    return function (callback, ms, uniqueId)
    {
        if (!uniqueId) {
          uniqueId = "Don't call this twice without a uniqueId";
        }
        if (timers[uniqueId])
        {
          clearTimeout (timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();